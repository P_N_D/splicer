import * as fs from 'fs';
import * as request from 'supertest';

import {hostingWithOGP, Config, Model} from '../src/handler/hosting-with-ogp';


const config: Config = {
  hosting: {
    domain: 'splicer.app',
    cacheMaxAgeSeconds: '300'
  }
};
const model: Model = {
  getClip: async (id: string) => id === 'invalid_id'
    ? undefined
    : {
      id,
      title: 'test clip title',
      description: 'test clip description',
      videoId: 'ADgodTeFaMM',
      startSeconds: 0,
      endSeconds: 269,
      thumbnailUrl: 'https://img.youtube.com/vi/ADgodTeFaMM/mqdefault.jpg',
      vtubers: {
        refs: [],
        datas: []
      },
      owner: undefined,
      visibility: 'public' as 'public' | 'limited',
      views: 100000000000,
      createdAt: new Date(),
      updatedAt: new Date()
    },

  getPlaylist: async (id: string) => id === 'invalid_id'
    ? undefined
    : {
      id,
      title: 'test playlist title',
      description: 'test playlist description',
      thumbnailUrl: 'https://img.youtube.com/vi/ADgodTeFaMM/mqdefault.jpg',
      vtubers: {
        refs: [],
        datas: []
      },
      owner: undefined,
      visibility: 'public' as 'public' | 'limited',
      views: 100000000000,
      createdAt: new Date(),
      updatedAt: new Date()
    }
};

const func = hostingWithOGP(config, model);


describe('Hosting with OGP', () => {

  describe('when User-Agent of the request is `twitterbot`', () => {
    it('returns HTML compatible with Twitter player card for `/clip/:id`', async () => {
      await request(func)
        .get('/clip/clip_id')
        .set('User-Agent', 'twitterbot')
        .expect('Vary', 'User-Agent')
        .expect('Cache-Control', `public, max-age=${config.hosting.cacheMaxAgeSeconds}`)
        .expect(200)
        .expect(res => {
          const lines = [
            `<meta name="twitter:player" content="https://${config.hosting.domain}/clip/embed/clip_id">`,
            `<meta property="og:url" content="https://${config.hosting.domain}/clip/clip_id"/>`,
            `<meta property="og:title" content="test clip title"/>`
          ];
          for (const line of lines) {
            expect(res.text).toContain(line);
          }
        });
    });

    it('returns HTML compatible with Twitter player card for `/pl/:id`', async () => {
      await request(func)
        .get('/pl/playlist_id')
        .set('User-Agent', 'twitterbot')
        .expect('Vary', 'User-Agent')
        .expect('Cache-Control', `public, max-age=${config.hosting.cacheMaxAgeSeconds}`)
        .expect(200)
        .expect(res => {
          const lines = [
            `<meta name="twitter:player" content="https://${config.hosting.domain}/pl/embed/playlist_id">`,
            `<meta property="og:url" content="https://${config.hosting.domain}/pl/playlist_id"/>`,
            `<meta property="og:title" content="test playlist title"/>`
          ];
          for (const line of lines) {
            expect(res.text).toContain(line);
          }
        });
    });

    it('returns HTML compatible with Twitter summary card if clip ID is invalid', async () => {
      await request(func)
        .get('/clip/invalid_id')
        .set('User-Agent', 'twitterbot')
        .expect('Vary', 'User-Agent')
        .expect('Cache-Control', `public, max-age=${config.hosting.cacheMaxAgeSeconds}`)
        .expect(200)
        .expect(res => {
          const lines = [
            `<meta name="twitter:card" content="summary">`,
            `<meta property="og:image" content="https://${config.hosting.domain}/assets/favicon/original.png"/>`
          ];
          for (const line of lines) {
            expect(res.text).toContain(line);
          }
        });
    });

    it('returns HTML compatible with Twitter summary card if playlist ID is invalid', async () => {
      await request(func)
        .get('/pl/invalid_id')
        .set('User-Agent', 'twitterbot')
        .expect('Vary', 'User-Agent')
        .expect('Cache-Control', `public, max-age=${config.hosting.cacheMaxAgeSeconds}`)
        .expect(200)
        .expect(res => {
          const lines = [
            `<meta name="twitter:card" content="summary">`,
            `<meta property="og:image" content="https://${config.hosting.domain}/assets/favicon/original.png"/>`
          ];
          for (const line of lines) {
            expect(res.text).toContain(line);
          }
        });
    });

    it('returns HTML compatible with Twitter summary card for any other routes', async () => {
      await request(func)
        .get('/arbitrary/path')
        .set('User-Agent', 'twitterbot')
        .expect('Vary', 'User-Agent')
        .expect('Cache-Control', `public, max-age=${config.hosting.cacheMaxAgeSeconds}`)
        .expect(200)
        .expect(res => {
          const lines = [
            `<meta name="twitter:card" content="summary">`,
            `<meta property="og:image" content="https://${config.hosting.domain}/assets/favicon/original.png"/>`
          ];
          for (const line of lines) {
            expect(res.text).toContain(line);
          }
        });
    });
  });

  describe('when User-Agent of the request is NOT `twitterbot`', () => {
    it('returns `views/index.html` as is', async () => {
      const body = fs.readFileSync('./views/index.html').toString();

      await request(func)
        .get('/arbitrary/path')
        .expect('Vary', 'User-Agent')
        .expect('Cache-Control', `public, max-age=${config.hosting.cacheMaxAgeSeconds}`)
        .expect(200, body);

      await request(func)
        .get('/clip/test_id')
        .expect('Vary', 'User-Agent')
        .expect('Cache-Control', `public, max-age=${config.hosting.cacheMaxAgeSeconds}`)
        .expect(200, body);
    });
  });
});
