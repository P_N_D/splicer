import {add, batch, collection, get, ref, update, value} from 'typesaurus';

import {Clip, ClipsInPlaylist, Playlist, User, VTuber, WatchHistory} from './schema';


interface Dates {
  createdAt: Date;
  updatedAt: Date;
}

type Remove<T, U> = Pick<T, Exclude<keyof T, keyof U>>

export const collections = {
  user: collection<User>('user'),
  vtuber: collection<VTuber>('vtuber'),
  clip: collection<Clip>('clip'),
  playlist: collection<Playlist>('playlist'),
  clipsInPlaylist: collection<ClipsInPlaylist>('clipsInPlaylist'),
  watchHistory: collection<WatchHistory>('watchHistory')
};

export const getClip = async (id: string): Promise<Clip | undefined> => {
  const clip = await get(collections.clip, id);
  return clip ? clip.data : undefined;
};

export const getPlaylist = async (id: string): Promise<Playlist | undefined> => {
  const playlist = await get(collections.playlist, id);
  return playlist ? playlist.data : undefined;
};

export const addUser = async (
  id: string,
  {
    name, avatarUrl, bio, providerData
  }: Remove<User, Dates>) => {

  const b = batch();
  b.set(collections.user, id, {
    name, avatarUrl, bio, providerData,
    createdAt: value('serverDate'),
    updatedAt: value('serverDate')
  });
  await b.commit();
};

export const addWatchClipHistory = async (clipId: string, ipAddress: string, userId?: string) => {
  await add(collections.watchHistory, {
    ipAddress,
    user: userId ? ref(collections.user, userId) : undefined,
    target: ref(collections.clip, clipId),
    createdAt: value('serverDate')
  });
};

export const addWatchPlaylistHistory = async (playlistId: string, ipAddress: string, userId?: string) => {
  await add(collections.watchHistory, {
    ipAddress,
    user: userId ? ref(collections.user, userId) : undefined,
    target: ref(collections.playlist, playlistId),
    createdAt: value('serverDate')
  });
};

export const incrementClipViewCount = async (id: string) => {
  await update(collections.clip, id, {
    views: value('increment', 1)
  });
};

export const incrementPlaylistViewCount = async (id: string) => {
  await update(collections.playlist, id, {
    views: value('increment', 1)
  });
};
