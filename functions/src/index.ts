import * as firebase from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as algoliasearch from 'algoliasearch';

import {onDocumentWritten} from './handler/copy-firestore-to-algolia';
import {onUserCreated} from './handler/copy-fireauth-to-firestore';
import {hostingWithOGP} from './handler/hosting-with-ogp';
import {addWatchHistory} from './handler/add-watch-history';
import {onNewWatchHistory} from './handler/update-view-count';

import * as model from './model';

firebase.initializeApp();

const config = functions.config().env;
const ALGOLIA_ID = config.algolia.app_id;
const ALGOLIA_ADMIN_KEY = config.algolia.api_key;
const algolia = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

exports.onVTuberWritten = onDocumentWritten(algolia, config);
exports.onUserCreated = onUserCreated();
exports.hostingWithOGP = hostingWithOGP(config, model);
exports.addWatchHistory = addWatchHistory();
exports.onNewWatchHistory = onNewWatchHistory(config);
