import * as firebase from 'firebase-admin';
import * as functions from 'firebase-functions';

import {incrementClipViewCount, incrementPlaylistViewCount} from '../model';


type DocumentReference = firebase.firestore.DocumentReference;

interface Config {
  algolia: { index_name: string }
  firestore: {
    collection_path: {
      watchHistory: string,
      clip: string,
      playlist: string
    }
  }
}

interface WatchHistory {
  target: DocumentReference;
}

export const onNewWatchHistory = (config: Config) =>
  functions.region('asia-northeast1').firestore
    .document(`${config.firestore.collection_path.watchHistory}/{id}`)
    .onCreate(async (snapshot, context) => {

      // TODO: Type check `doc`.
      const doc = snapshot.data() as WatchHistory | undefined;

      if (!doc) {
        return;
      }

      const firestoreDocRef = doc.target;
      const collectionPath = firestoreDocRef.parent.path;
      const id = firestoreDocRef.id;

      switch (collectionPath) {
        case config.firestore.collection_path.clip:
          return incrementClipViewCount(id);

        case config.firestore.collection_path.playlist:
          return incrementPlaylistViewCount(id);

        default:
          throw new Error(`The path '${collectionPath}' neither references to clip collection nor playlist collection.`)
      }
    });
