import * as functions from 'firebase-functions';
import * as express from 'express';
import * as  mustacheExpress from 'mustache-express';
import {resolve} from 'path';

import * as _model from '../model';


export interface Config {
  hosting: {
    domain: string;
    cacheMaxAgeSeconds: string
  }
}

export type Model = Pick<typeof _model, 'getClip' | 'getPlaylist'>;

function initializeApp(config: Config, model: Model) {
  const app = express();
  app.engine('html', mustacheExpress());
  app.set('view engine', 'html');
  app.set('views', './views');

  app.get('*', (req, res, next) => {
    // Refer to the following page about the 'Vary' header.
    // https://firebase.google.com/docs/hosting/manage-cache#vary_headers
    res.header('Vary', 'User-Agent');
    res.header('Cache-Control', `public, max-age=${config.hosting.cacheMaxAgeSeconds}`);

    const _userAgent = req.headers['user-agent'];
    const userAgent = _userAgent ? _userAgent.toLowerCase() : '';

    userAgent.includes('twitterbot')
      ? next()
      : res.sendFile(resolve('./views/index.html'));
  });

  app.get('/clip/:id', async (req, res, next) => {
    const id = req.params.id;
    const clip = await model.getClip(id);

    !clip
      ? next()
      : res.render('twitter_ogp_player', {
        id,
        title: clip.title,
        domain: config.hosting.domain,
        entity: 'clip',
        thumbnailUrl: clip.thumbnailUrl,
      });
  });

  app.get('/pl/:id', async (req, res, next) => {
    const id = req.params.id;
    const playlist = await model.getPlaylist(id);

    !playlist
      ? next()
      : res.render('twitter_ogp_player', {
        id,
        title: playlist.title,
        domain: config.hosting.domain,
        entity: 'pl',
        thumbnailUrl: playlist.thumbnailUrl
      });
  });

  app.get('*', (req, res) => {
    res.sendFile(resolve('./views/index.html'));
  });

  return app;
}

export const hostingWithOGP =
  (config: Config, model: Model) => functions.https.onRequest(initializeApp(config, model));
