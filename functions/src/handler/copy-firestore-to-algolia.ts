import * as algoliasearch from 'algoliasearch';
import * as functions from 'firebase-functions';


interface Config {
  algolia: { index_name: string }
  firestore: { collection_path: { vtuber: string } }
}

export const onDocumentWritten = (algolia: algoliasearch.Client, config: Config) =>
  functions.region('asia-northeast1').firestore
    .document(`${config.firestore.collection_path.vtuber}/{id}`)
    .onWrite((change, context) => {

      const documentId = change.before.id;
      const firestoreDocument = change.after.data();
      const index = algolia.initIndex(config.algolia.index_name);

      if (firestoreDocument) {
        // The document has been created or updated.
        const algoliaRecord = {objectID: documentId, ...firestoreDocument};
        return index.saveObject(algoliaRecord);
      } else {
        // The document has been deleted.
        return index.deleteObject(documentId)
      }
    });
