import * as functions from 'firebase-functions';

import {addUser} from '../model';

export const onUserCreated = () =>
  functions.region('asia-northeast1').auth.user().onCreate(async user => {

    const providerData = user.providerData.map(
      ({displayName, providerId, uid, photoURL}) => ({
        uid, providerId,
        name: displayName,
        avatarUrl: photoURL
      })
    );
    const userDocument = {
      providerData,
      name: user.displayName || '',
      avatarUrl: user.photoURL
    };

    await addUser(user.uid, userDocument);
  });
