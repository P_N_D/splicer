import * as functions from 'firebase-functions';

import {addWatchClipHistory, addWatchPlaylistHistory} from '../model';

interface validArgument {
  entity: 'clip' | 'playlist';
  id: string;
}

function isValidArgument(data: any): data is validArgument {
  return ['clip', 'playlist'].includes(data.entity)
    && typeof data.id === 'string';
}

export const addWatchHistory = () =>
  functions.region('asia-northeast1').https.onCall(async (data, context) => {
    if (!isValidArgument(data)) {
      throw new functions.https.HttpsError('invalid-argument', `The given argument is invalid: ${data}`);
    }

    const userId = context.auth ? context.auth.uid : undefined;
    const ipAddress = context.rawRequest.ip;

    try {
      if (data.entity === 'clip') {
        const clipId = data.id;
        await addWatchClipHistory(clipId, ipAddress, userId);
      } else {
        const playlistId = data.id;
        await addWatchPlaylistHistory(playlistId, ipAddress, userId);
      }
    } catch {
      throw new functions.https.HttpsError('internal', 'A DB operation failed.');
    }

  });
