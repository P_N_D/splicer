import {Component, Input, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map} from 'rxjs/operators';
import {of, Observable} from 'rxjs';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  @Input() sidenavOpened: boolean = false;
  @Input() sidenavMode: 'over' | 'push' | 'side' | null = null;

  private isSmallScreen$: Observable<boolean> = this.breakpointObserver
    .observe([Breakpoints.XSmall, Breakpoints.Small])
    .pipe(map(state => state.matches));

  sidenavMode$ = this.sidenavMode
    ? of(this.sidenavMode)
    : this.isSmallScreen$.pipe(map(isSmall => isSmall ? 'over' : 'side'));

  sidenavOpened$ = this.isSmallScreen$.pipe(
    map(isSmall => !isSmall && this.sidenavOpened)
  );

  constructor(private breakpointObserver: BreakpointObserver) {
  }

  ngOnInit() {
  }
}
