import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FlexLayoutModule} from '@angular/flex-layout';

import {ContainerComponent} from './container.component';
import {NavbarComponent} from '../navbar/navbar.component';
import {SidenavComponent} from '../sidenav/sidenav.component';
import {MatListModule} from '@angular/material/list';


@NgModule({
  declarations: [
    ContainerComponent,
    NavbarComponent,
    SidenavComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatSidenavModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatButtonModule,
    MatTooltipModule,
    MatListModule,
    FlexLayoutModule,
  ],
  exports: [ContainerComponent]
})
export class ContainerModule {
}
