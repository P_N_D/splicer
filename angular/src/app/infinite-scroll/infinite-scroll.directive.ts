import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';


interface Context<T> {
  $implicit: T;
}

type Paginator<T> = Iterator<T[]> | AsyncIterator<T[]>;

@Directive({
  selector: '[appInfiniteScroll]'
})
export class InfiniteScrollDirective<T> {
  private iterator: Paginator<T>;

  @Input() set appInfiniteScrollOf(iterator: Paginator<T>) {
    this.viewContainer.clear();
    this.iterator = iterator;
  }

  constructor(
    private template: TemplateRef<Context<T>>,
    private viewContainer: ViewContainerRef) {
  }

  loadNext = async () => {
    const {value: items, done} = await this.iterator.next();

    if (!done) {
      for (const item of items) {
        this.viewContainer.createEmbeddedView(this.template, {$implicit: item});
      }
    }
  }
}
