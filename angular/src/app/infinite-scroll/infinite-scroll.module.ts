import {NgModule} from '@angular/core';

import {InfiniteScrollContainerDirective} from './infinite-scroll-container.directive';
import {InfiniteScrollDirective} from './infinite-scroll.directive';


@NgModule({
  declarations: [
    InfiniteScrollContainerDirective,
    InfiniteScrollDirective,
  ],
  exports: [
    InfiniteScrollContainerDirective,
    InfiniteScrollDirective
  ]
})
export class InfiniteScrollModule {
}
