import {ContentChild, Directive, ElementRef, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {InfiniteScrollDirective} from './infinite-scroll.directive';
import {fromEvent, merge, Observable} from 'rxjs';
import {debounceTime, exhaustMap, filter, map, startWith, tap} from 'rxjs/operators';


function observeMutation(target: HTMLElement, config: MutationObserverInit): Observable<MutationRecord[]> {
  return new Observable(observer => {
    const mutation = new MutationObserver(record => observer.next(record));
    mutation.observe(target, config);
    return () => mutation.disconnect();
  });
}

function scrollRatio(element: HTMLElement): number {
  const currentScroll = element.scrollTop;
  const maxScroll = element.scrollHeight - element.clientHeight;
  const value = maxScroll === 0
    ? 1
    : currentScroll / maxScroll;

  return Math.min(Math.max(value, 0), 1);
}


@Directive({
  selector: '[appInfiniteScrollContainer]'
})
export class InfiniteScrollContainerDirective<T> implements OnDestroy {
  constructor(private elem: ElementRef) {
  }

  @Input() public thresholdRatio = 0.9;
  @Output() loading = new EventEmitter<boolean>();

  @ContentChild(InfiniteScrollDirective, {static: true})
  directive: InfiniteScrollDirective<T>;

  private scrolled$ = fromEvent<Event>(this.elem.nativeElement, 'scroll');
  private resized$ = fromEvent<Event>(window, 'resize');
  private elementMutated$ = observeMutation(this.elem.nativeElement, {
    childList: true, // Monitor the target node for the addition or removal of child nodes.
    subtree: true // Monitor the entire subtree.
  });

  private bottomReached$ = merge(this.scrolled$, this.resized$, this.elementMutated$).pipe(
    startWith(undefined),
    debounceTime(100),
    map(() => this.scrollRatio()),
    filter(ratio => ratio >= this.thresholdRatio)
  );
  private loaded$ = this.bottomReached$.pipe(
    tap(() => this.loading.emit(true)),
    exhaustMap(() => this.loadNext()),        // Ignore emissions until the promise returned by `loadNext` is resolved.
    tap(() => this.loading.emit(false))
  );
  private subscription = this.loaded$.subscribe();

  private loadNext = () => this.directive.loadNext();

  private scrollRatio = () => scrollRatio(this.elem.nativeElement);

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.loading.complete();
  }

}
