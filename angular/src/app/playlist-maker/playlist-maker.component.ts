import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Validators} from '@angular/forms';
import {FormBuilder} from 'ngx-strongly-typed-forms';

import {ModelService, Doc, Playlist} from '../model/model.service';
import {maxTextLengthValidator} from '../shared/validators';


@Component({
  selector: 'app-playlist-maker',
  templateUrl: './playlist-maker.component.html',
  styleUrls: ['./playlist-maker.component.css']
})
export class PlaylistMakerComponent implements OnInit {
  @Output() submitted = new EventEmitter<Doc<Playlist> | null>();

  formGroup = this.fb.group<{ title: string, visibility: 'public' | 'limited' }>({
    title: ['', [Validators.required, maxTextLengthValidator(60)]],
    visibility: ['public']
  });

  constructor(private fb: FormBuilder, private model: ModelService) {
  }

  async onSubmit() {
    if (this.formGroup.invalid) {
      return;
    }
    const playlist = this.formGroup.value;

    try {
      const doc = await this.model.addPlaylist(playlist);
      this.submitted.emit(doc);
    } catch {
      this.submitted.emit(null);
    }
  }

  ngOnInit() {
  }

}
