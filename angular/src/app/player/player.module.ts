import {NgModule} from '@angular/core';
import {PlayerComponent} from './player.component';
import {YouTubePlayerModule} from '../youtube-player/youtube-player.module';


@NgModule({
  imports: [YouTubePlayerModule],
  exports: [PlayerComponent],
  declarations: [PlayerComponent],
})
export class PlayerModule {
}
