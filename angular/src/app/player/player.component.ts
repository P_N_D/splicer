import {Component, OnInit, Inject, Input, ViewChild, EventEmitter, Output, OnDestroy} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {distinctUntilChanged, map, skip} from 'rxjs/operators';

import {YouTubePlayerComponent} from '../youtube-player/youtube-player.component';
import {Subscription} from 'rxjs';

export interface Clip {
  videoId?: string;
  startSeconds?: number;
  endSeconds?: number;
}

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit, OnDestroy {
  @Input() clip: Clip;
  @Input() playerVars: YT.PlayerVars = {
    modestbranding: YT.ModestBranding.Modest,
    playsinline: YT.PlaysInline.Inline
  };

  @Output() ended = new EventEmitter<void>();
  @Output() playing = new EventEmitter<void>();
  @Output() paused = new EventEmitter<void>();
  @Output() buffering = new EventEmitter<void>();
  @Output() cued = new EventEmitter<void>();
  @Output() unstarted = new EventEmitter<void>();
  @Output() watched = new EventEmitter<void>();

  @ViewChild(YouTubePlayerComponent) player: YouTubePlayerComponent;

  private subscriptions: Subscription[] = [];

  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  getCurrentTime(): number {
    return this.player.getCurrentTime();
  }

  onStateChange({data}: YT.OnStateChangeEvent) {
    if (data === YT.PlayerState.ENDED) {
      // YouTube IFrame API sometimes emits 'ENDED' event even though the video is not played.
      // See: https://stackoverflow.com/a/54287913
      const isReallyEnded = this.getCurrentTime() >= this.clip.endSeconds;
      isReallyEnded && this.ended.emit();
    } else {
      const stateToEventMap = new Map<YT.PlayerState, EventEmitter<void>>([
        [YT.PlayerState.PLAYING, this.playing],
        [YT.PlayerState.PAUSED, this.paused],
        [YT.PlayerState.BUFFERING, this.buffering],
        [YT.PlayerState.CUED, this.cued],
        [YT.PlayerState.UNSTARTED, this.unstarted]
      ]);
      stateToEventMap.get(data).emit();
    }
  }

  ngOnInit() {
    const tag = this.document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    this.document.body.appendChild(tag);

    const subscription1 = this.playing.pipe(
      map(() => this.clip),
      distinctUntilChanged()
    ).subscribe(() => this.watched.emit());

    const subscription2 = this.cued.pipe(
      skip(1)
    ).subscribe(() => this.player.playVideo());

    this.subscriptions = [subscription1, subscription2];
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
