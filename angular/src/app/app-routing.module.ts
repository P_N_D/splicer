import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AuthGuard} from './auth/auth.guard';
import {PlaylistOwnerGuard} from './auth/playlist-owner.guard';


const routes: Routes = [
  {path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)},
  {path: 'about', loadChildren: () => import('./about/about.module').then(m => m.AboutModule)},
  {path: 'privacy', loadChildren: () => import('./privacy-policy/privacy-policy.module').then(m => m.PrivacyPolicyModule)},
  {path: 'contact', loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule)},
  {
    path: 'me',
    loadChildren: () => import('./mypage/mypage.module').then(m => m.MypageModule),
    canActivate: [AuthGuard]
  },
  {path: 'clip/new', loadChildren: () => import('./clip-maker/clip-maker.module').then(m => m.ClipMakerModule)},
  {path: 'clip/embed/:id', loadChildren: () => import('./embed-clip/embed-clip.module').then(m => m.EmbedClipModule)},
  {path: 'clip/:id', loadChildren: () => import('./clip-viewer/clip-viewer.module').then(m => m.ClipViewerModule)},
  {path: 'pl/embed/:id', loadChildren: () => import('./embed-playlist/embed-playlist.module').then(m => m.EmbedPlaylistModule)},
  {path: 'pl/:id', loadChildren: () => import('./playlist-viewer/playlist-viewer.module').then(m => m.PlaylistViewerModule)},
  {
    path: 'pl/edit/:id',
    loadChildren: () => import('./playlist-editor/playlist-editor.module').then(m => m.PlaylistEditorModule),
    canActivate: [PlaylistOwnerGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
