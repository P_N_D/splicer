import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

export interface Clip {
  id: string;
  title: string;
  thumbnailUrl: string;
}

@Component({
  selector: 'app-playlist-sorter',
  templateUrl: './playlist-sorter.component.html',
  styleUrls: ['./playlist-sorter.component.scss']
})
export class PlaylistSorterComponent implements OnInit {
  @Input() clips: Clip[];
  @Output() clipsChange = new EventEmitter<Clip[]>();
  @Output() thumbnailChange = new EventEmitter<Clip>();
  @Output() clipRemoved = new EventEmitter<Clip>();

  constructor() {
  }

  drop(event: CdkDragDrop<Clip[]>) {
    moveItemInArray(this.clips, event.previousIndex, event.currentIndex);
    this.clipsChange.emit(this.clips);
  }

  ngOnInit() {
  }
}
