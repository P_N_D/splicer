import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistSorterComponent } from './playlist-sorter.component';

describe('PlaylistSorterComponent', () => {
  let component: PlaylistSorterComponent;
  let fixture: ComponentFixture<PlaylistSorterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistSorterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistSorterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
