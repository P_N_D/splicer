import {Component, ElementRef, OnDestroy, OnInit, Optional, Self, ViewChild} from '@angular/core';
import {ControlValueAccessor, FormGroupDirective, NgControl, NgForm} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatFormFieldControl } from '@angular/material/form-field';
import {FocusMonitor} from '@angular/cdk/a11y';
import {FormControl} from 'ngx-strongly-typed-forms';
import {BehaviorSubject} from 'rxjs';
import {map} from 'rxjs/operators';

import {CustomMatFormField} from '../shared/custom-mat-form-field';


const invalidTimeChars = /[^0-9:]/g;

function timeStringToSeconds(str: string): number {
  const _str = str.replace(invalidTimeChars, '');
  const [h, m, s] = _str.split(':').map(x => parseInt(x.slice(0, 2), 10));

  return (h || 0 % 24) * 60 * 60 + (m || 0 % 60) * 60 + (s || 0 % 60);
}

function secondsToTimeString(seconds: number): string {
  const s = seconds % (24 * 60 * 60);
  return new Date(s * 1000).toISOString().substr(11, 8);
}

@Component({
  selector: 'app-time-input-field',
  template: '<input #inputElem matInput autocomplete="off" [formControl]="formControl" (keydown)="disableDeletion($event)" (input)="onUserInput($event)">',
  styles: [],
  providers: [{provide: MatFormFieldControl, useExisting: TimeInputFieldComponent}]
})
export class TimeInputFieldComponent extends CustomMatFormField<number>
  implements OnInit, OnDestroy, ControlValueAccessor, MatFormFieldControl<number> {

  formControl = new FormControl<string>();

  value$ = new BehaviorSubject(0);

  @ViewChild('inputElem', {static: true, read: ElementRef}) inputElem: ElementRef;

  get shouldLabelFloat(): boolean {
    return true;
  }

  constructor(
    @Optional() @Self() ngControl: NgControl,
    fm: FocusMonitor,
    elRef: ElementRef<HTMLElement>,
    @Optional() _parentForm: NgForm,
    @Optional() _parentFormGroup: FormGroupDirective,
    _defaultErrorStateMatcher: ErrorStateMatcher) {

    super(ngControl, fm, elRef, _parentForm, _parentFormGroup, _defaultErrorStateMatcher);
  }

  disableDeletion(event: KeyboardEvent) {
    const digitRegex = /^\d$/;
    const arrowRegex = /^Arrow(?:Left|Right)$/;

    if (!digitRegex.test(event.key) && !arrowRegex.test(event.code) && event.key !== 'Tab') {
      event.preventDefault();
    }
  }

  onUserInput(event) {
    this.value = timeStringToSeconds(event.target.value);
  }

  private setFormValue(value: string) {
    const input = this.inputElem.nativeElement;
    const start = input.selectionStart;
    const end = input.selectionEnd;

    this.formControl.setValue(value);
    input.setSelectionRange(start, end);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.value$
      .pipe(map(secondsToTimeString))
      .subscribe(x => this.setFormValue(x));
  }
}
