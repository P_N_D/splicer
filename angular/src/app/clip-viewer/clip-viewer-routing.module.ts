import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ClipViewerComponent} from './clip-viewer.component';


const routes: Routes = [{path: '', component: ClipViewerComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClipViewerRoutingModule {
}
