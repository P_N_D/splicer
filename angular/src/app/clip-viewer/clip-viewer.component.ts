import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {Observable} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

import {ModelService} from '../model/model.service';
import {Clip as Clip1} from '../clip-info/clip-info.component';
import {Clip as Clip2} from '../player/player.component';
import {Clip as Clip3} from '../related-clips/related-clips.component';

type Clip = Clip1 & Clip2 & Clip3;

@Component({
  selector: 'app-clip-viewer',
  templateUrl: './clip-viewer.component.html',
  styleUrls: ['./clip-viewer.component.css']
})
export class ClipViewerComponent implements OnInit {
  clip: Observable<Clip> = this.route.paramMap.pipe(
    map(paramMap => paramMap.get('id') as string),
    switchMap(this.model.getClip)
  );

  constructor(
    private model: ModelService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title) {
  }

  async addWatchHistory(clipId: string) {
    try {
      await this.model.addWatchClipHistory(clipId);
    } catch {
      console.error('Failed to save watch history.');
    }
  }

  async ngOnInit() {
    this.clip.subscribe(clip => {
      if (clip) {
        this.title.setTitle(`${clip.title} - Splicer`);
      } else {
        this.router.navigate(['']);
      }
    });
  }
}
