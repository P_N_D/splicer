import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';

import {ClipViewerComponent} from './clip-viewer.component';
import {ClipViewerRoutingModule} from './clip-viewer-routing.module';
import {PlayerModule} from '../player/player.module';
import {RelatedClipsModule} from '../related-clips/related-clips.module';
import {ClipInfoModule} from '../clip-info/clip-info.module';
import {ContainerModule} from '../container/container.module';


@NgModule({
  declarations: [
    ClipViewerComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ClipViewerRoutingModule,
    PlayerModule,
    RelatedClipsModule,
    ClipInfoModule,
    ContainerModule
  ]
})
export class ClipViewerModule {
}
