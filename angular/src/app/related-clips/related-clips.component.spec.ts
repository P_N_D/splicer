import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedClipsComponent } from './related-clips.component';

describe('RelatedClipsComponent', () => {
  let component: RelatedClipsComponent;
  let fixture: ComponentFixture<RelatedClipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedClipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedClipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
