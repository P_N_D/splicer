import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import {RelatedClipsComponent} from './related-clips.component';
import {InfiniteScrollModule} from '../infinite-scroll/infinite-scroll.module';


@NgModule({
  declarations: [
    RelatedClipsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    MatTooltipModule,
    MatToolbarModule,
    MatListModule,
    MatProgressSpinnerModule,
    InfiniteScrollModule,
  ],
  exports: [RelatedClipsComponent]
})
export class RelatedClipsModule {
}
