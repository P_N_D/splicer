import {Component, Input, OnInit} from '@angular/core';

import {ModelService} from '../model/model.service';
import {Clip as ClipCard} from '../clip-card/clip-card.component';
import {mapAsyncIterator} from '../shared/utils';


export interface Clip {
  id: string;
  vtubers: { id: string }[];
}

@Component({
  selector: 'app-related-clips',
  templateUrl: './related-clips.component.html',
  styleUrls: ['./related-clips.component.scss']
})
export class RelatedClipsComponent implements OnInit {
  @Input() clip: Clip;
  private vtuberIds: string[];
  relatedClips: AsyncIterator<Array<ClipCard & { id: string }>>;
  isLoading = false;

  constructor(private model: ModelService) {
  }

  ngOnInit() {
    this.vtuberIds = this.clip.vtubers.map(vtuber => vtuber.id);
    this.relatedClips =
      mapAsyncIterator(
        this.model.listClipsOfVTubers(this.vtuberIds, 10),
        clips => clips.map(
          clip => ({...clip, url: `/clip/${clip.id}`})
        )
      );
  }

}
