import {Component, Inject, OnInit} from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import {Doc, ModelService, Playlist} from '../model/model.service';
import {AuthService} from '../auth/auth.service';


@Component({
  selector: 'app-playlist-save-dialog',
  templateUrl: './playlist-save-dialog.component.html',
  styleUrls: ['./playlist-save-dialog.component.css']
})
export class PlaylistSaveDialogComponent implements OnInit {
  private clipId = this.data.clipId;
  myPlaylists = this.getPlaylists();
  isMakingPlaylist = false;
  user = this.auth.currentUser;

  constructor(
    private model: ModelService,
    private snackBar: MatSnackBar,
    private auth: AuthService,
    @Inject(MAT_DIALOG_DATA) private data: { clipId: string }) {
  }

  private parsePlaylist = (data: { id: string, title: string, clips: { id: string }[] }) => ({
    id: data.id,
    title: data.title,
    isClipSaved: data.clips.some(clip => clip.id === this.clipId)
  });

  async getPlaylists() {
    const playlists = await this.model.listPlaylistsDetailsOfCurrentUser();
    return playlists.map(this.parsePlaylist);
  }

  async addOrRemoveFromPlaylist(event: MatCheckboxChange) {
    const playlistId: string = event.source.value;
    const action = event.checked
      ? this.model.saveClipToPlaylist
      : this.model.removeClipFromPlaylist;
    const successMessage = event.checked
      ? 'この切り抜きをプレイリストに保存しました。'
      : 'この切り抜きをプレイリストから削除しました。';

    try {
      await action(this.clipId, playlistId);
      this.snackBar.open(successMessage, 'OK', {duration: 3000});
    } catch {
      event.source.toggle();
      this.snackBar.open(
        'プレイリストへの保存に失敗しました。時間を置いて再度お試しください。',
        'OK', {duration: 10000}
      );
    }
  }

  async openPlaylistMaker() {
    this.isMakingPlaylist = true;
  }

  onPlaylistCreated(doc?: Doc<Playlist>) {
    this.isMakingPlaylist = false;

    if (doc) {
      const playlist = {
        id: doc.ref.id,
        title: doc.data.title,
        isClipSaved: false
      };
      this.myPlaylists = this.myPlaylists.then(x => x.concat(playlist));
      this.snackBar.open(
        'プレイリストを作成しました。',
        'OK', {duration: 3000}
      );
    } else {
      this.snackBar.open(
        'プレイリストの作成に失敗しました。時間を置いて再度お試しください。',
        'OK', {duration: 10000}
      );
    }
  }

  async signIn() {
    await this.auth.signInWithTwitter();
  }

  ngOnInit() {
  }

}
