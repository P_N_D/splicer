import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistSaveDialogComponent } from './playlist-save-dialog.component';

describe('PlaylistSaveDialogComponent', () => {
  let component: PlaylistSaveDialogComponent;
  let fixture: ComponentFixture<PlaylistSaveDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistSaveDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistSaveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
