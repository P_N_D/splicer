import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClipMakerComponent} from './clip-maker.component';


const routes: Routes = [{path: '', component: ClipMakerComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClipMakerRoutingModule {
}
