import {AbstractControl, FormGroup} from 'ngx-strongly-typed-forms';
import {FileInput} from 'ngx-material-file-input';
import {FormControl, FormGroupDirective, NgForm, ValidationErrors} from '@angular/forms';

import {extractYouTubeVideoId} from '../shared/utils';
import { ErrorStateMatcher } from '@angular/material/core';

export interface VTuber {
  id: string;
  name: string;
  avatarUrl: string;
}

export interface TimeRange {
  startSeconds: number;
  endSeconds: number;
}

export interface ModelWithoutUrl {
  timeRange: TimeRange;
  description: string;
  title: string;
  visibility: 'public' | 'limited';
  vtubers: VTuber[];
  thumbnail: FileInput;
}

export function timeRangeValidator(control: FormGroup<TimeRange>): ValidationErrors | null {
  const {startSeconds, endSeconds} = control.value;
  return startSeconds < endSeconds ? null : {invalidTimeRange: [startSeconds, endSeconds]};
}

export function videoUrlValidator({value}: AbstractControl<string>): ValidationErrors | null {
  return extractYouTubeVideoId(value) ? null : {invalidUrlFormat: value};
}

export class CrossFieldErrorMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return control.parent.touched && control.parent.invalid;
  }
}
