import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipMakerComponent } from './clip-maker.component';

describe('ClipMakerComponent', () => {
  let component: ClipMakerComponent;
  let fixture: ComponentFixture<ClipMakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClipMakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipMakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
