import {Component, OnInit, ViewChild} from '@angular/core';
import {Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {Overlay} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {MatSpinner} from '@angular/material/progress-spinner';
import {combineLatest} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, startWith, tap} from 'rxjs/operators';
import {FormBuilder} from 'ngx-strongly-typed-forms';
import {ImageCroppedEvent} from 'ngx-image-cropper';

import {PlayerComponent} from '../player/player.component';
import {extractYouTubeVideoId} from '../shared/utils';
import {ModelService} from '../model/model.service';
import {CrossFieldErrorMatcher, ModelWithoutUrl, TimeRange, timeRangeValidator, videoUrlValidator} from './clip-maker.validators';
import {maxLengthValidator, maxTextLengthValidator, minLengthValidator} from '../shared/validators';
import {base64toBlob} from '../shared/utils';


@Component({
  selector: 'app-clip-maker',
  templateUrl: './clip-maker.component.html',
  styleUrls: ['./clip-maker.component.css']
})
export class ClipMakerComponent implements OnInit {
  @ViewChild(PlayerComponent) player: PlayerComponent;

  urlGroup = this.fb.group<{ videoUrl: string }>({
    videoUrl: ['', videoUrlValidator]
  });

  formGroup = this.fb.group<ModelWithoutUrl>({
    description: ['', maxTextLengthValidator(280)],
    title: ['', [Validators.required, maxTextLengthValidator(60)]],
    thumbnail: [undefined, []],
    visibility: ['public'],
    vtubers: [[], [minLengthValidator(1), maxLengthValidator(10)]],
    timeRange: this.fb.group<TimeRange>({
      startSeconds: [0],
      endSeconds: [0]
    }, {
      validators: timeRangeValidator
    })
  });

  timeRangeErrorMatcher = new CrossFieldErrorMatcher();

  thumbnailFile = this.formGroup.valueChanges.pipe(
    map(model => model.thumbnail
      ? model.thumbnail.files[0]
      : undefined
    ),
    startWith(undefined)
  );

  croppedThumbnail: Blob | null | undefined = undefined;

  clipChanges = combineLatest(this.urlGroup.valueChanges, this.formGroup.valueChanges).pipe(
    map(([{videoUrl}, {timeRange: {startSeconds, endSeconds}}]) => ({
      videoId: extractYouTubeVideoId(videoUrl),
      startSeconds,
      endSeconds
    })),
    filter(({videoId}) => !!videoId),
    distinctUntilChanged((x, y) =>
      x.videoId === y.videoId &&
      x.startSeconds === y.startSeconds &&
      x.endSeconds === y.endSeconds
    ),
    debounceTime(1000)
  );

  overlayRef = this.overlay.create({
    hasBackdrop: true,
    positionStrategy: this.overlay
      .position().global().centerHorizontally().centerVertically()
  });

  setStartSeconds() {
    const startSeconds = this.player.getCurrentTime();
    this.formGroup.controls.timeRange.patchValue({startSeconds});
  }

  setEndSeconds() {
    const endSeconds = this.player.getCurrentTime();
    this.formGroup.controls.timeRange.patchValue({endSeconds});
  }

  imageCropped(event: ImageCroppedEvent) {
    const base64 = event.base64.split(',')[1];
    this.croppedThumbnail = base64toBlob(base64);
  }

  loadImageFailed() {
    this.openSnackBar('画像の読み込みに失敗しました。ファイルが破損している可能性があります');
  }

  private openSnackBar(message) {
    this.snackBar.open(message, 'OK', {duration: 3000});
  }

  async onSubmit() {
    if (this.formGroup.invalid) {
      this.openSnackBar('正しく入力されていない項目があります');
      return;
    }

    const {videoUrl} = this.urlGroup.value;
    const {timeRange: {startSeconds, endSeconds}, ...rest} = this.formGroup.value;
    const videoId = extractYouTubeVideoId(videoUrl);

    this.overlayRef.attach(new ComponentPortal(MatSpinner));
    try {
      const {ref: {id}} = await this.model.addClip({
        ...rest,
        videoId,
        startSeconds,
        endSeconds,
        thumbnail: this.croppedThumbnail
      });
      this.openSnackBar('切り抜きを作成しました');
      await this.router.navigate([`/clip/${id}`]);
    } catch {
      this.openSnackBar('切り抜きの作成に失敗しました。時間を置いて再度お試しください。');
    } finally {
      this.overlayRef.detach();
    }
  }

  constructor(
    private fb: FormBuilder,
    private model: ModelService,
    private router: Router,
    private snackBar: MatSnackBar,
    private title: Title,
    private overlay: Overlay) {
  }

  ngOnInit() {
    this.title.setTitle(`切り抜きの新規作成 - Splicer`);
  }
}
