import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {NgxStronglyTypedFormsModule} from 'ngx-strongly-typed-forms';
import {ImageCropperModule} from 'ngx-image-cropper';

import {ClipMakerRoutingModule} from './clip-maker-routing.module';
import {PlayerModule} from '../player/player.module';
import {ContainerModule} from '../container/container.module';
import {ClipMakerComponent} from './clip-maker.component';
import {MultiSelectVtubersComponent} from '../multi-select-vtubers/multi-select-vtubers.component';
import {TimeInputFieldComponent} from '../time-input-field/time-input-field.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


@NgModule({
  declarations: [
    ClipMakerComponent,
    MultiSelectVtubersComponent,
    TimeInputFieldComponent,
  ],
  imports: [
    CommonModule,
    ClipMakerRoutingModule,
    FlexLayoutModule,
    MatStepperModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatChipsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    NgxStronglyTypedFormsModule,
    MaterialFileInputModule,
    ImageCropperModule,
    ContainerModule,
    PlayerModule,
  ],
  providers: [{
    provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
    useValue: {appearance: 'outline'}
  }],
})
export class ClipMakerModule {
}
