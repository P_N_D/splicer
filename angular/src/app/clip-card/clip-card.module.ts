import {NgModule} from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {RouterModule} from '@angular/router';

import {VtuberChipModule} from '../vtuber-chip/vtuber-chips.module';
import {ClipCardComponent} from './clip-card.component';


@NgModule({
  declarations: [ClipCardComponent],
  imports: [
    MatCardModule,
    RouterModule,
    VtuberChipModule
  ],
  exports: [ClipCardComponent]
})
export class ClipCardModule {
}
