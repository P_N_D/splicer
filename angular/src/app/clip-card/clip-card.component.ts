import {Component, Input, OnInit} from '@angular/core';


interface VTuber {
  name: string;
  avatarUrl: string;
  youtubeId: string;
}

export interface Clip {
  url: string;
  title: string;
  thumbnailUrl?: string;
  vtubers: VTuber[];
}

@Component({
  selector: 'app-clip-card',
  templateUrl: './clip-card.component.html',
  styleUrls: ['./clip-card.component.css']
})
export class ClipCardComponent implements OnInit {
  @Input() clip: Clip;
  thumbnailUrl: string;

  constructor() {
  }

  ngOnInit() {
    this.thumbnailUrl = this.clip.thumbnailUrl || 'assets/placeholder/320x180.png';
  }

}
