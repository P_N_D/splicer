import {NgModule} from '@angular/core';
import {YouTubePlayerComponent} from './youtube-player.component';


@NgModule({
  declarations: [YouTubePlayerComponent],
  exports: [YouTubePlayerComponent]
})
export class YouTubePlayerModule {
}
