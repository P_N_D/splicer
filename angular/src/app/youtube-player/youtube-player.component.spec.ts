import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YouTubePlayerComponent } from './youtube-player.component';

describe('YoutubePlayerComponent', () => {
  let component: YouTubePlayerComponent;
  let fixture: ComponentFixture<YouTubePlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YouTubePlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YouTubePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
