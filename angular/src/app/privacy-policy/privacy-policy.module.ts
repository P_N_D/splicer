import {NgModule} from '@angular/core';

import {ContainerModule} from '../container/container.module';
import {PrivacyPolicyRoutingModule} from './privacy-policy-routing.module';
import {PrivacyPolicyComponent} from './privacy-policy.component';


@NgModule({
  declarations: [
    PrivacyPolicyComponent,
  ],
  imports: [
    PrivacyPolicyRoutingModule,
    ContainerModule,
  ]
})
export class PrivacyPolicyModule {
}
