import { DoCheck, ElementRef, HostBinding, Input, OnDestroy, OnInit, Optional, Self, Directive } from '@angular/core';
import {ControlValueAccessor, FormGroupDirective, NgControl, NgForm} from '@angular/forms';
import {FocusMonitor} from '@angular/cdk/a11y';
import { CanUpdateErrorStateCtor, ErrorStateMatcher, mixinErrorState } from '@angular/material/core';
import { MatFormFieldControl } from '@angular/material/form-field';
import {BehaviorSubject, combineLatest} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import * as uuid from 'short-uuid';


// Boilerplate for applying mixin.
class Base {
  constructor(public _defaultErrorStateMatcher: ErrorStateMatcher,
              public _parentForm: NgForm,
              public _parentFormGroup: FormGroupDirective,
              public ngControl: NgControl) {
  }
}

const _MixinBase: CanUpdateErrorStateCtor & typeof Base =
  mixinErrorState(Base);

/*
  This abstract class is used to implement a custom form-field component which:
    - is compatible with Angular forms API.
    - can be used inside Angular Material's <mat-form-field> component.

  Usage:
    1. Extend this class.
    2. Define the property `value$` to be a BehaviorSubject with a proper initial value.
    3. You may override other properties and methods if required.

  Refer to the following documents for details:
    - https://angular.io/api/forms/ControlValueAccessor
    - https://material.angular.io/guide/creating-a-custom-form-field-control
 */
@Directive()
export abstract class CustomMatFormField<T>
  extends _MixinBase
  implements OnInit, OnDestroy, DoCheck, ControlValueAccessor, MatFormFieldControl<T> {

  @Input() errorStateMatcher: ErrorStateMatcher;

  @HostBinding() id = uuid().new();
  @HostBinding('attr.aria-describedby') describedBy = '';

  protected constructor(
    public ngControl: NgControl | null,
    private fm: FocusMonitor,
    private elRef: ElementRef<HTMLElement>,
    _parentForm: NgForm,
    _parentFormGroup: FormGroupDirective,
    _defaultErrorStateMatcher: ErrorStateMatcher
  ) {
    super(_defaultErrorStateMatcher, _parentForm, _parentFormGroup, ngControl);

    if (this.ngControl != null) {
      // Setting the values$ accessor directly (instead of using
      // the providers) to avoid running into a circular import.
      this.ngControl.valueAccessor = this;
    }
  }

  abstract value$: BehaviorSubject<T>;
  private _placeholder = new BehaviorSubject<string>('');
  private _required = new BehaviorSubject<boolean>(false);
  private _disabled = new BehaviorSubject<boolean>(false);
  private _focused = new BehaviorSubject<boolean>(false);

  private onChange = (_: any) => {
  };
  private onTouched = (_: any) => {
  };

  get value(): T {
    return this.value$.value;
  }

  set value(val: T) {
    this.value$.next(val);
  }

  get placeholder() {
    return this._placeholder.value;
  }

  set placeholder(val) {
    this._placeholder.next(val);
  }

  get required() {
    return this._required.value;
  }

  set required(val) {
    this._required.next(val);
  }

  get disabled() {
    return this._disabled.value;
  }

  set disabled(val) {
    this._disabled.next(val);
  }

  get focused() {
    return this._focused.value;
  }

  get empty() {
    return !!this.value;
  }

  @HostBinding('class.floating')
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  onContainerClick(event: MouseEvent): void {
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnInit(): void {
    combineLatest(this.value$, this._placeholder, this._required, this._disabled, this._focused)
      .pipe(map(_ => null))
      .subscribe(this.stateChanges);

    this.value$.subscribe(this.onChange);

    const fm = this.fm.monitor(this.elRef.nativeElement, true);

    fm.pipe(map(focusOrigin => focusOrigin !== null))
      .subscribe(this._focused);

    fm.pipe(filter(focusOrigin => focusOrigin === null))
      .subscribe(this.onTouched);
  }

  ngOnDestroy() {
    const subjects = [
      this.stateChanges,
      this.value$,
      this._placeholder,
      this._required,
      this._disabled,
      this._focused
    ];

    for (const subject of subjects) {
      subject.complete();
    }

    this.fm.stopMonitoring(this.elRef.nativeElement);
  }

  ngDoCheck() {
    if (this.ngControl) {
      // We need to re-evaluate this on every change detection cycle, because there are some
      // error triggers that we can't subscribe to (e.g. parent form submissions). This means
      // that whatever logic is in here has to be super lean or we risk destroying the performance.
      this.updateErrorState();
    }
  }
}
