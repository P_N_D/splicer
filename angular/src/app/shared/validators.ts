import {AbstractControl} from 'ngx-strongly-typed-forms';

import {textLength} from './utils';


export function minLengthValidator(n: number) {
  return ({value}: AbstractControl<{ length: number }>) => {
    return value.length >= n ? null : {lengthTooShort: value};
  };
}

export function maxLengthValidator(n: number) {
  return ({value}: AbstractControl<{ length: number }>) => {
    return value.length <= n ? null : {lengthTooLong: value};
  };
}

export function maxTextLengthValidator(n: number) {
  return ({value}: AbstractControl<string>) => {
    return textLength(value) <= n ? null : {textLengthTooLong: value};
  };
}

