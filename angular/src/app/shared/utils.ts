import * as parseUrl from 'url-parse';


const youtubeVideoIdRegExp = RegExp('([a-zA-Z0-9\\-_])+');

export function isValidYouTubeVideoId(text: string): boolean {
  return youtubeVideoIdRegExp.test(text);
}

export function extractYouTubeVideoId(url: string): string | null {
  const {origin, query, pathname} = parseUrl(url, true);

  if (
    origin === 'https://www.youtube.com' ||
    origin === 'https://youtube.com'
  ) {
    return (pathname === '/watch' && isValidYouTubeVideoId(query.v))
      ? query.v
      : null;
  }

  if (
    origin === 'https://www.youtu.be' ||
    origin === 'https://youtu.be'
  ) {
    const pathWithoutSlash = (pathname as string).substring(1);
    return isValidYouTubeVideoId(pathWithoutSlash)
      ? pathWithoutSlash
      : null;
  }

  return null;
}

export function textLength(text: string) {
  return text.length;
}

export function mapAsyncIterator<T, S>(iter: AsyncIterator<T>, f: (value: T) => S): AsyncIterator<S> {
  return {
    async next() {
      const {value, done} = await iter.next();
      return {
        done,
        value: done ? undefined : f(value)
      };
    }
  };
}

export function uniq<T>(array: Array<T>): Array<T> {
  return [...new Set(array)];
}

export function base64toBlob(base64: string, contentType = '', sliceSize = 512): Blob {
  const byteCharacters = atob(base64);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  return new Blob(byteArrays, {type: contentType});
}
