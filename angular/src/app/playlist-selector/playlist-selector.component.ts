import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ModelService} from '../model/model.service';


export interface Clip {
  title: string;
  thumbnailUrl: string;
}

interface Playlist {
  id: string;
  title: string;
  owner: {
    id: string;
    name: string;
  };
}

@Component({
  selector: 'app-playlist-selector',
  templateUrl: './playlist-selector.component.html',
  styleUrls: ['./playlist-selector.component.scss']
})
export class PlaylistSelectorComponent implements OnInit {
  @Input() playlist: Playlist;
  @Input() clips: Clip[];
  @Input() index: number;
  @Input() repeat: BehaviorSubject<boolean>;
  @Input() shuffle: BehaviorSubject<boolean>;
  @Output() indexChange = new EventEmitter();

  user = this.model.currentUser();

  toggleRepeat() {
    this.repeat.next(!this.repeat.value);
  }

  toggleShuffle() {
    this.shuffle.next(!this.shuffle.value);
  }

  select(i: number) {
    this.index = i;
    this.indexChange.emit(i);
  }

  constructor(private model: ModelService) {
  }

  async ngOnInit() {
  }

}
