import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatTabsModule} from '@angular/material/tabs';

import {MypageRoutingModule} from './mypage-routing.module';
import {MypageComponent} from './mypage.component';
import {ContainerModule} from '../container/container.module';
import {ClipsGridListModule} from '../clips-grid-list/clips-grid-list.module';


@NgModule({
  declarations: [
    MypageComponent,
  ],
  imports: [
    CommonModule,
    MypageRoutingModule,
    MatTabsModule,
    ContainerModule,
    ClipsGridListModule,
  ]
})
export class MypageModule {
}
