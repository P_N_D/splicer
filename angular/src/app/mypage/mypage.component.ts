import {Component, OnInit} from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';

import {ModelService} from '../model/model.service';
import {mapAsyncIterator} from '../shared/utils';
import {Clip} from '../clip-card/clip-card.component';


@Component({
  selector: 'app-mypage',
  templateUrl: './mypage.component.html',
  styleUrls: ['./mypage.component.css']
})
export class MypageComponent implements OnInit {
  clips: AsyncIterator<Clip[]>;
  playlists: AsyncIterator<Clip[]>;

  private clipsPromise = async () => {
    const iterator = await this.model.listClipsOfCurrentUser(10);
    return mapAsyncIterator(iterator, clips =>
      clips.map(clip => ({...clip, url: `/clip/${clip.id}`}))
    );
  }

  private playlistsPromise = async () => {
    const iterator = await this.model.listPlaylistsOfCurrentUser(10);
    return mapAsyncIterator(iterator, playlists =>
      playlists.map(playlist => ({...playlist, url: `/pl/edit/${playlist.id}`}))
    );
  }

  async tabChanged(event: MatTabChangeEvent) {
    if (event.index === 0) {
      this.clips = await this.clipsPromise();
    }
    if (event.index === 1) {
      this.playlists = await this.playlistsPromise();
    }
  }

  constructor(private model: ModelService) {
  }

  async ngOnInit() {
    this.clips = await this.clipsPromise();
  }

}
