import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {ContainerModule} from '../container/container.module';
import {ClipsGridListModule} from '../clips-grid-list/clips-grid-list.module';
import {HomeComponent} from './home.component';


@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ContainerModule,
    ClipsGridListModule,
  ]
})
export class HomeModule {
}
