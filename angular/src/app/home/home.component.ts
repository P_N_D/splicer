import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

import {mapAsyncIterator} from '../shared/utils';
import {ModelService} from '../model/model.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  clips = this.getClips();

  async getClips() {
    const iterator = await this.model.listRecentClips(10);
    return mapAsyncIterator(iterator, clips =>
      clips.map(clip => ({...clip, url: `/clip/${clip.id}`}))
    );
  }

  constructor(private model: ModelService, private title: Title) {
    title.setTitle('Splicer');
  }

  ngOnInit() {
  }

}
