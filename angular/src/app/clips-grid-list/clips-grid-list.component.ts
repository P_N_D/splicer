import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';

import {Clip} from '../clip-card/clip-card.component';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-clips-grid-list',
  templateUrl: './clips-grid-list.component.html',
  styleUrls: ['./clips-grid-list.component.css']
})
export class ClipsGridListComponent implements OnInit, OnDestroy {
  @Input() clipPages: Iterator<Clip[]> | AsyncIterator<Clip[]>;
  isLoading = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.isLoading.complete();
  }
}
