import {NgModule} from '@angular/core';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {CommonModule} from '@angular/common';

import {ClipCardModule} from '../clip-card/clip-card.module';
import {ClipsGridListComponent} from './clips-grid-list.component';
import {InfiniteScrollModule} from '../infinite-scroll/infinite-scroll.module';


@NgModule({
  declarations: [
    ClipsGridListComponent,
  ],
  imports: [
    CommonModule,
    ClipCardModule,
    MatProgressSpinnerModule,
    InfiniteScrollModule,
  ],
  exports: [ClipsGridListComponent]
})
export class ClipsGridListModule {
}
