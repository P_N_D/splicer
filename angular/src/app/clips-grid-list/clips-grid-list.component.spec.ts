import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipsGridListComponent } from './clips-grid-list.component';

describe('ClipsGridListComponent', () => {
  let component: ClipsGridListComponent;
  let fixture: ComponentFixture<ClipsGridListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClipsGridListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipsGridListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
