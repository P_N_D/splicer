import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PlaylistViewerComponent} from './playlist-viewer.component';


const routes: Routes = [{path: '', component: PlaylistViewerComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistViewerRoutingModule {
}
