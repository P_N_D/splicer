import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {ShareModule} from '@ngx-share/core';

import {PlaylistViewerComponent} from './playlist-viewer.component';
import {ContainerModule} from '../container/container.module';
import {PlayerModule} from '../player/player.module';
import {ClipInfoModule} from '../clip-info/clip-info.module';
import {PlaylistSelectorComponent} from '../playlist-selector/playlist-selector.component';
import {RelatedClipsModule} from '../related-clips/related-clips.module';


@NgModule({
  declarations: [
    PlaylistViewerComponent,
    PlaylistSelectorComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    MatTooltipModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    ShareModule,
    ContainerModule,
    PlayerModule,
    ClipInfoModule,
    RelatedClipsModule,
  ]
})
export class PlaylistViewerModule {
}
