import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {BehaviorSubject, combineLatest, from} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

import {ModelService} from '../model/model.service';
import {PlaylistState} from './playlist-state';


@Component({
  selector: 'app-playlist-viewer',
  templateUrl: './playlist-viewer.component.html',
  styleUrls: ['./playlist-viewer.component.css']
})
export class PlaylistViewerComponent implements OnInit, OnDestroy {
  private playlistId: string = this.route.snapshot.paramMap.get('id');
  playlist = this.model.getPlaylist(this.playlistId);
  clips = this.playlist.then(playlist => playlist.clips);
  index = new BehaviorSubject(0);
  repeat = new BehaviorSubject(false);
  shuffle = new BehaviorSubject(false);
  private playlistState: Promise<PlaylistState> = this.getPlaylistState();

  clip =
    combineLatest(from(this.clips), this.index).pipe(
      map(([clips, i]) => clips[i]),
      switchMap(clip => this.model.getClip(clip.id))
    );

  constructor(
    private model: ModelService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title) {
  }

  async selectClip(i: number) {
    const state = await this.playlistState;
    state.select(i);
    this.index.next(i);
  }

  async loadNextClip() {
    const state = await this.playlistState;
    const i = state.playNext();
    if (i !== null) {
      this.index.next(i);
    }
  }

  async getPlaylistState() {
    const n = (await this.clips).length;
    return new PlaylistState(n, false, false);
  }

  async addWatchHistory() {
    try {
      const clips = await this.clips;
      const clip = clips[this.index.value];
      await this.model.addWatchClipHistory(clip.id);
      await this.model.addWatchPlaylistHistory(this.playlistId);
    } catch (e) {
      console.error('Failed to save watch history.', e);
    }
  }

  async ngOnInit() {
    const state = await this.playlistState;
    this.shuffle.subscribe(value => {
      state.isShuffleEnabled = value;
    });
    this.repeat.subscribe(value => {
      state.isRepeatEnabled = value;
    });

    const playlist = await this.playlist;
    this.title.setTitle(`${playlist.title} - Splicer`);

    if (!playlist) {
      await this.router.navigate(['']);
    }
  }

  ngOnDestroy() {
    const subjects = [this.index, this.repeat, this.shuffle];

    for (const subject of subjects) {
      subject.complete();
    }
  }
}
