export class PlaylistState {
  constructor(
    private numberOfItems: number,
    private _isShuffleEnabled: boolean = false,
    public isRepeatEnabled: boolean = false,
    private shuffledQueue: Array<number> = shuffle(range(numberOfItems)),
    private _index: number = 0
  ) {
  }

  get index(): number {
    return this._index;
  }

  set index(val) {
    this._index = val;
    this.shuffledQueue = this.shuffledQueue.filter(i => i !== val);
  }

  get isShuffleEnabled(): boolean {
    return this._isShuffleEnabled;
  }

  set isShuffleEnabled(value) {
    this._isShuffleEnabled = value;
    if (value) {
      this.initShuffledQueue();
    }
  }

  private initShuffledQueue() {
    const array = shuffle(range(this.numberOfItems));
    if (this.index === array[0]) {
      const m = array.length - 1;
      [array[0], array[m]] = [array[m], array[0]];
    }
    this.shuffledQueue = array;
    this.index = this.shuffledQueue[0];
  }

  private init(): void {
    if (!this.isShuffleEnabled) {
      this.index = 0;
      return;
    }
    this.initShuffledQueue();
  }

  public isFinished(): boolean {
    return this.isShuffleEnabled
      ? this.shuffledQueue.length === 0
      : this.index + 1 >= this.numberOfItems;
  }

  public playNext(): number | null {
    const fin = this.isFinished();
    const repeat = this.isRepeatEnabled;

    if (fin && !repeat) {
      return null;
    }

    if (fin && repeat) {
      this.init();
      return this.index;
    }

    this.index = this.isShuffleEnabled
      ? this.shuffledQueue[0]
      : this.index + 1;
    return this.index;
  }

  public select(index: number): void {
    this.index = index;
  }
}


function range(n: number): Array<number> {
  return Array.from(Array(n).keys());
}

function shuffle<T>(array: Array<T>): Array<T> {
  const _array = array.slice();
  for (let i = _array.length - 1; i >= 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [_array[i], _array[j]] = [_array[j], _array[i]];
  }
  return _array;
}
