import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MatSelectChange } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Title} from '@angular/platform-browser';
import {first, map, switchMap} from 'rxjs/operators';

import {ModelService} from '../model/model.service';
import {Clip} from '../playlist-sorter/playlist-sorter.component';
import {FormControl} from 'ngx-strongly-typed-forms';


@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent implements OnInit {
  private playlistId: string = this.route.snapshot.paramMap.get('id');
  playlist = this.model.onGetPlaylist(this.playlistId);
  isEditingTitle = false;

  constructor(
    private model: ModelService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private title: Title) {
  }

  showErrorMessage = () => {
    this.snackBar.open(
      'エラーが発生しました。時間を置いて再度お試しください。',
      'OK', {duration: 10000}
    );
  };

  async clipsChange(clips: Clip[]) {
    const clipIds = clips.map(clip => clip.id);

    await this.model.reorderPlaylist(this.playlistId, clipIds)
      .catch(this.showErrorMessage);
  }

  async clipRemoved(clip: { id: string, title: string }) {
    try {
      await this.model.removeClipFromPlaylist(clip.id, this.playlistId);
      this.snackBar.open(
        `『${clip.title}』をプレイリストから削除しました。`,
        'OK', {duration: 5000}
      );
    } catch {
      this.showErrorMessage();
    }
  }

  async thumbnailChange(clip: { thumbnailUrl: string, title: string }) {
    try {
      await this.model.editPlaylist(this.playlistId, {thumbnailUrl: clip.thumbnailUrl});
      this.snackBar.open(
        `『${clip.title}』のサムネイルをプレイリストのサムネイルに設定しました。`,
        'OK', {duration: 5000}
      );
    } catch {
      this.showErrorMessage();
    }
  }

  async titleChange(title: string) {
    try {
      await this.model.editPlaylist(this.playlistId, {title});
      this.snackBar.open(
        `プレイリストのタイトルを変更しました。`,
        'OK', {duration: 5000}
      );
    } catch {
      this.showErrorMessage();
    } finally {
      this.isEditingTitle = false;
    }
  }

  async accessPolicyChange({value}: MatSelectChange) {
    try {
      await this.model.editPlaylist(this.playlistId, {visibility: value});
      this.snackBar.open(
        `プレイリストの公開設定を変更しました。`,
        'OK', {duration: 5000}
      );
    } catch {
      this.showErrorMessage();
    }
  }

  async ngOnInit() {
    this.playlist.pipe(first())
      .subscribe(playlist => {
        if (playlist) {
          this.title.setTitle(`プレイリストの編集 - ${playlist.title} - Splicer`);
        } else {
          this.router.navigate(['']);
        }
      });
  }
}
