import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {DragDropModule} from '@angular/cdk/drag-drop';

import {ContainerModule} from '../container/container.module';
import {PlaylistEditorComponent} from './playlist-editor.component';
import {PlaylistSorterComponent} from '../playlist-sorter/playlist-sorter.component';
import {PlaylistEditorRoutingModule} from './playlist-editor-routing.module';


@NgModule({
  declarations: [
    PlaylistEditorComponent,
    PlaylistSorterComponent,
  ],
  imports: [
    CommonModule,
    PlaylistEditorRoutingModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatSelectModule,
    MatListModule,
    MatMenuModule,
    DragDropModule,
    ContainerModule,
  ]
})
export class PlaylistEditorModule {
}
