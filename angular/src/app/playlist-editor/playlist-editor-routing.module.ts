import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PlaylistEditorComponent} from './playlist-editor.component';


const routes: Routes = [{path: '', component: PlaylistEditorComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistEditorRoutingModule {
}
