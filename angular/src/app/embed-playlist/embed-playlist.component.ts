import {Component, OnInit} from '@angular/core';

import {ModelService} from '../model/model.service';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject, combineLatest, from} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';


@Component({
  selector: 'app-embed-playlist',
  templateUrl: './embed-playlist.component.html',
  styleUrls: ['./embed-playlist.component.css']
})
export class EmbedPlaylistComponent implements OnInit {
  private playlistId: string = this.route.snapshot.paramMap.get('id');
  playlist = this.model.getPlaylist(this.playlistId);
  clips = this.playlist.then(playlist => playlist.clips);
  index = new BehaviorSubject(0);
  clip =
    combineLatest(from(this.clips), this.index).pipe(
      map(([clips, i]) => clips[i]),
      switchMap(clip => this.model.getClip(clip.id))
    );

  constructor(
    private model: ModelService,
    private route: ActivatedRoute) {
  }

  async loadNextClip() {
    this.index.next(this.index.value + 1);
  }

  async addWatchHistory() {
    try {
      const clips = await this.clips;
      const clip = clips[this.index.value];
      await this.model.addWatchClipHistory(clip.id);
      await this.model.addWatchPlaylistHistory(this.playlistId);
    } catch (e) {
      console.error('Failed to save watch history.', e);
    }
  }

  ngOnInit() {
  }

}
