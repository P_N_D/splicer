import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {EmbedPlaylistComponent} from './embed-playlist.component';


const routes: Routes = [{path: '', component: EmbedPlaylistComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmbedPlaylistRoutingModule {
}
