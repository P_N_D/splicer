import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PlayerModule} from '../player/player.module';
import {EmbedPlaylistComponent} from './embed-playlist.component';
import {EmbedPlaylistRoutingModule} from './embed-playlist-routing.module';


@NgModule({
  declarations: [
    EmbedPlaylistComponent,
  ],
  imports: [
    EmbedPlaylistRoutingModule,
    CommonModule,
    PlayerModule,
  ]
})
export class EmbedPlaylistModule {
}
