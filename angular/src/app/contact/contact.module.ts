import {NgModule} from '@angular/core';

import {ContainerModule} from '../container/container.module';
import {ContactRoutingModule} from './contact-routing.module';
import {ContactComponent} from './contact.component';


@NgModule({
  declarations: [
    ContactComponent,
  ],
  imports: [
    ContactRoutingModule,
    ContainerModule,
  ]
})
export class ContactModule {
}
