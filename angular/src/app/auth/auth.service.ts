import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import 'firebase/auth';


interface User {
  id: string;
  name: string;
  email: string;
  avatarUrl?: string;
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private fireAuth: AngularFireAuth,
    private router: Router,
    private snackBar: MatSnackBar) {
  }

  private twitterAuthProvider = new firebase.auth.TwitterAuthProvider();

  currentUser: Observable<User | null> = this.fireAuth.authState.pipe(
    map(user =>
      !user ?
        null :
        {
          id: user.uid,
          name: user.displayName,
          email: user.email,
          avatarUrl: user.photoURL
        }
    )
  );

  async currentUserPromise(): Promise<User | null> {
    return this.currentUser.pipe(take(1)).toPromise();
  }

  async signInWithTwitter(): Promise<void> {
    await this.fireAuth.auth.signInWithRedirect(this.twitterAuthProvider);
    await this.snackBar.open('ログインしました。', 'OK', {duration: 3000});
  }

  async signOut(): Promise<void> {
    await this.fireAuth.auth.signOut();
    await this.router.navigate(['']);
    await this.snackBar.open('ログアウトしました。', 'OK', {duration: 3000});
  }
}
