import { TestBed, async, inject } from '@angular/core/testing';

import { PlaylistOwnerGuard } from './playlist-owner.guard';

describe('PlaylistOwnerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlaylistOwnerGuard]
    });
  });

  it('should ...', inject([PlaylistOwnerGuard], (guard: PlaylistOwnerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
