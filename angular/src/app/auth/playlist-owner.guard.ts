import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';

import {ModelService} from '../model/model.service';


@Injectable({
  providedIn: 'root'
})
export class PlaylistOwnerGuard implements CanActivate {
  constructor(
    private model: ModelService,
    private router: Router) {
  }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean | UrlTree> {

    const user = await this.model.currentUser();
    const playlistId = next.paramMap.get('id');
    const playlist = playlistId ? await this.model.getPlaylist(playlistId) : null;
    const isAllowed = !!user && !!playlist && playlist.owner.id === user.id;

    return isAllowed || this.router.parseUrl('');
  }
}
