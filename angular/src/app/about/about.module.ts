import {NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';

import {AboutRoutingModule} from './about-routing.module';
import {AboutComponent} from './about.component';
import {ContainerModule} from '../container/container.module';


@NgModule({
  declarations: [
    AboutComponent,
  ],
  imports: [
    AboutRoutingModule,
    ContainerModule,
    MatButtonModule,
  ]
})
export class AboutModule {
}
