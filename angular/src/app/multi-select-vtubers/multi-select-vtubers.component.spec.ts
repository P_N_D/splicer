import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiSelectVtubersComponent } from './multi-select-vtubers.component';

describe('MultiSelectVtubersComponent', () => {
  let component: MultiSelectVtubersComponent;
  let fixture: ComponentFixture<MultiSelectVtubersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiSelectVtubersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectVtubersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
