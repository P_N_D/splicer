import {Component, ElementRef, OnDestroy, OnInit, Optional, Self, ViewChild} from '@angular/core';
import {ControlValueAccessor, FormGroupDirective, NgControl, NgForm} from '@angular/forms';
import {FocusMonitor} from '@angular/cdk/a11y';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatFormFieldControl } from '@angular/material/form-field';
import {FormControl} from 'ngx-strongly-typed-forms';
import {BehaviorSubject, combineLatest} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, startWith, switchMap} from 'rxjs/operators';

import {CustomMatFormField} from '../shared/custom-mat-form-field';
import {ModelService} from '../model/model.service';


function differenceBy<S, T>(array1: Array<S>, array2: Array<T>, key: string): Array<S> {
  return array1.filter(obj1 => array2.every(obj2 => obj1[key] !== obj2[key]));
}

export interface VTuber {
  id: string;
  name: string;
  avatarUrl: string;
}

@Component({
  selector: 'app-multi-select',
  templateUrl: './multi-select-vtubers.component.html',
  styleUrls: ['./multi-select-vtubers.component.css'],
  providers: [{provide: MatFormFieldControl, useExisting: MultiSelectVtubersComponent}]
})
export class MultiSelectVtubersComponent extends CustomMatFormField<VTuber[]>
  implements OnInit, OnDestroy, ControlValueAccessor, MatFormFieldControl<VTuber[]> {

  constructor(
    private model: ModelService,
    @Optional() @Self() ngControl: NgControl,
    fm: FocusMonitor,
    elRef: ElementRef<HTMLElement>,
    @Optional() _parentForm: NgForm,
    @Optional() _parentFormGroup: FormGroupDirective,
    _defaultErrorStateMatcher: ErrorStateMatcher) {

    super(ngControl, fm, elRef, _parentForm, _parentFormGroup, _defaultErrorStateMatcher);
  }

  value$ = new BehaviorSubject<VTuber[]>([]);

  @ViewChild('vtuberInput') inputFormElement: ElementRef<HTMLInputElement>;

  inputFormControl = new FormControl<string>();
  private searchResults = this.inputFormControl.valueChanges.pipe(
    startWith([]),
    filter((text: string) => text.length > 1),
    debounceTime(500),
    distinctUntilChanged(),
    switchMap((text: string) =>
      this.model.searchVTubersByName(text, 10).catch(() => [])
    )
  );
  filteredVTubers = combineLatest(this.searchResults, this.value$).pipe(
    map(([searchResults, selected]) =>
      differenceBy(searchResults, selected, 'id')
    )
  );

  addSelection({option: {value}}: MatAutocompleteSelectedEvent): void {
    // `this.inputFormControl.setValue` won't work due to a bug of Angular:
    // https://github.com/angular/components/issues/10968
    this.inputFormElement.nativeElement.value = '';

    this.value = this.value.concat([value]);
  }

  removeSelection(vtuber: VTuber): void {
    this.value = differenceBy(this.value, [vtuber], 'id');
  }

  display(vtuber: VTuber | null | undefined): string {
    return vtuber ? vtuber.name : '';
  }

  get empty() {
    return this.value.length === 0;
  }
}
