import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {ShareModule} from '@ngx-share/core';
import {FlexLayoutModule} from '@angular/flex-layout';

import {VtuberChipModule} from '../vtuber-chip/vtuber-chips.module';
import {ClipInfoComponent} from './clip-info.component';
import {PlaylistSaveDialogComponent} from '../playlist-save-dialog/playlist-save-dialog.component';
import {PlaylistMakerComponent} from '../playlist-maker/playlist-maker.component';


@NgModule({
  declarations: [
    ClipInfoComponent,
    PlaylistSaveDialogComponent,
    PlaylistMakerComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    MatTooltipModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSelectModule,
    ShareModule,
    FlexLayoutModule,
    VtuberChipModule,
  ],
  exports: [ClipInfoComponent]
})
export class ClipInfoModule {
}
