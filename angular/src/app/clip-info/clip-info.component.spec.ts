import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipInfoComponent } from './clip-info.component';

describe('ClipInfoComponent', () => {
  let component: ClipInfoComponent;
  let fixture: ComponentFixture<ClipInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClipInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
