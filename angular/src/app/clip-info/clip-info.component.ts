import {Component, Input, OnInit, Inject} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {DOCUMENT} from '@angular/common';

import {PlaylistSaveDialogComponent} from '../playlist-save-dialog/playlist-save-dialog.component';


interface Identity {
  id: string;
}

interface User {
  name: string;
  avatarUrl?: string;
  bio?: string;
  twitter?: Identity;
}

interface VTuber {
  name: string;
  avatarUrl: string;
  bio?: string;
  twitterId: string;
  youtubeId: string;
}

export interface Clip {
  id: string;
  title: string;
  createdAt: Date;
  description: string;
  owner?: User;
  vtubers: VTuber[];
  views: number;
}


@Component({
  selector: 'app-clip-info',
  templateUrl: './clip-info.component.html',
  styleUrls: ['./clip-info.component.css']
})
export class ClipInfoComponent implements OnInit {
  @Input() clip: Clip;
  clipUrl: string;

  constructor(
    private dialog: MatDialog,
    @Inject(DOCUMENT) private document: Document) {
  }

  openDialog() {
    this.dialog.open(PlaylistSaveDialogComponent, {
      data: {clipId: this.clip.id}
    });
  }

  ngOnInit() {
    const origin = this.document.location.origin;
    this.clipUrl = `${origin}/clip/${this.clip.id}`;
  }

}
