import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {EmbedClipComponent} from './embed-clip.component';


const routes: Routes = [{path: '', component: EmbedClipComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmbedClipRoutingModule {
}
