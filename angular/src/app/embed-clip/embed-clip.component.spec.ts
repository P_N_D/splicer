import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbedClipComponent } from './embed-clip.component';

describe('EmbedClipComponent', () => {
  let component: EmbedClipComponent;
  let fixture: ComponentFixture<EmbedClipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbedClipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbedClipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
