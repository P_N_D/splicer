import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {ModelService} from '../model/model.service';


@Component({
  selector: 'app-embed-clip',
  templateUrl: './embed-clip.component.html',
  styleUrls: ['./embed-clip.component.css']
})
export class EmbedClipComponent implements OnInit {
  private clipId: string = this.route.snapshot.paramMap.get('id');
  clip = this.model.getClip(this.clipId);

  constructor(
    private model: ModelService,
    private route: ActivatedRoute) {
  }

  async addWatchHistory(clipId: string) {
    try {
      await this.model.addWatchClipHistory(clipId);
    } catch {
      console.error('Failed to save watch history.');
    }
  }

  ngOnInit() {
  }

}
