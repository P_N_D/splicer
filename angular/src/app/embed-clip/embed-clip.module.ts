import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PlayerModule} from '../player/player.module';
import {EmbedClipComponent} from './embed-clip.component';
import {EmbedClipRoutingModule} from './embed-clip-routing.module';


@NgModule({
  declarations: [
    EmbedClipComponent
  ],
  imports: [
    EmbedClipRoutingModule,
    CommonModule,
    PlayerModule,
  ]
})
export class EmbedClipModule {
}
