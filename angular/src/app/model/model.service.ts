import {Injectable} from '@angular/core';
import {AngularFireStorage} from '@angular/fire/storage';
import * as uuid from 'short-uuid';

import {AlgoliaService} from './algolia.service';
import {AuthService} from '../auth/auth.service';
import {FirebaseFunctionsService} from './firebase-functions.service';
import * as fsr from './firestore-read';
import * as fsw from './firestore-write';

import {VTuber} from './schema';

export {Doc} from 'typesaurus';
export {Playlist} from './schema';


@Injectable({
  providedIn: 'root'
})
export class ModelService {
  constructor(
    private algolia: AlgoliaService,
    private auth: AuthService,
    private fns: FirebaseFunctionsService,
    private storage: AngularFireStorage) {
  }

  getClip = fsr.getClip;
  getPlaylist = fsr.getPlaylist;
  onGetPlaylist = fsr.onGetPlaylist;

  editPlaylist = fsw.updatePlaylist;
  reorderPlaylist = fsw.reorderPlaylist;
  deletePlaylist = fsw.deletePlaylist;
  saveClipToPlaylist = fsw.saveClipToPlaylist;
  removeClipFromPlaylist = fsw.removeClipFromPlaylist;

  listClipsOfVTubers = fsr.listClipsOfVTubers;
  listRecentClips = fsr.listRecentClips;

  listPlaylistsDetailsOfCurrentUser = async () => {
    const user = await this.currentUser();
    return user ? fsr.listPlaylistsDetailsOfUser(user.id) : undefined;
  }

  listPlaylistsOfCurrentUser = async (maxNumResults: number) => {
    const user = await this.currentUser();
    return user ? fsr.listPlaylistsOfUser(user.id, maxNumResults) : undefined;
  }

  listClipsOfCurrentUser = async (maxNumResults: number) => {
    const user = await this.currentUser();
    return user ? fsr.listClipsOfUser(user.id, maxNumResults) : undefined;
  }

  addClip = async (data: {
    videoId: string,
    startSeconds: number,
    endSeconds: number,
    title: string,
    description: string,
    visibility: 'public' | 'limited',
    vtubers: { id: string }[],
    thumbnail?: Blob
  }) => {
    const {thumbnail, ...rest} = data;
    let thumbnailUrl: string;
    if (thumbnail) {
      const path = `/thumbnail/${uuid().new()}.jpg`;
      const ref = this.storage.ref(path);
      const task = await this.storage.upload(path, thumbnail);
      thumbnailUrl = await ref.getDownloadURL().toPromise();
    } else {
      thumbnailUrl = `https://img.youtube.com/vi/${data.videoId}/mqdefault.jpg`;
    }

    const user = await this.currentUser();
    const owner = user ? {id: user.id} : undefined;

    return fsw.addClip({...rest, owner, thumbnailUrl});
  }

  addPlaylist = async (data: {
    title: string,
    visibility: 'public' | 'limited',
  }) => {
    const user = await this.currentUser();
    const owner = user ? {id: user.id} : undefined;
    return fsw.addPlaylist({...data, owner});
  }

  addWatchClipHistory = async (id: string) =>
    this.fns.addWatchHistory({entity: 'clip', id})


  addWatchPlaylistHistory = async (id: string) =>
    this.fns.addWatchHistory({entity: 'playlist', id})

  searchVTubersByName = (name: string, maxNumResults: number): Promise<Array<VTuber & {id: string}>> =>
    this.algolia.searchVtubersByName(name, maxNumResults)

  currentUser = () => this.auth.currentUserPromise();
}
