import {add, batch, collection, field, get, Ref, ref, set, transaction, update, value} from 'typesaurus';

import {Clip, ClipsInPlaylist, Playlist, User, VTuber, WatchHistory} from './schema';
import {doesNotContainNull, removeById, pickByIds, reorder} from './utils';
import {uniq} from '../shared/utils';


const MAX_NUM_CLIPS_IN_PLAYLIST = 100;

export const collections = {
  user: collection<User>('user'),
  vtuber: collection<VTuber>('vtuber'),
  clip: collection<Clip>('clip'),
  playlist: collection<Playlist>('playlist'),
  clipsInPlaylist: collection<ClipsInPlaylist>('clipsInPlaylist'),
  watchHistory: collection<WatchHistory>('watchHistory')
};

export const checkUser = async (user?: { id: string }):
  Promise<{ ref: Ref<User>, data: User } | undefined> => {

  if (user) {
    const userDoc = await get(collections.user, user.id);

    if (userDoc === null) {
      throw new Error(`The specified user ID is invalid: ${user.id}`);
    } else {
      return {
        ref: ref(collections.user, user.id),
        data: userDoc.data
      };
    }
  } else {
    return undefined;
  }
};

export const addClip = async (data: {
  owner?: { id: string },
  videoId: string,
  startSeconds: number,
  endSeconds: number,
  title: string,
  description: string,
  visibility: 'public' | 'limited',
  vtubers: { id: string }[],
  thumbnailUrl: string
}) => {
  const {owner, vtubers, ...rest} = data;

  const vtuberRefs = vtubers.map(vtuber => ref(collections.vtuber, vtuber.id));
  const vtuberDocs = await Promise.all(vtuberRefs.map(reference => get<VTuber>(reference)));

  if (!doesNotContainNull(vtuberDocs)) {
    throw new Error(`Some of the specified VTuber IDs are invalid: ${vtubers.map(x => x.id)}`);
  }

  return add(collections.clip, {
    owner: await checkUser(owner),
    vtubers: {
      refs: vtuberRefs,
      datas: vtuberDocs.map(doc => doc.data)
    },
    views: 0,
    createdAt: value('serverDate'),
    updatedAt: value('serverDate'),
    ...rest
  });
};

export const addPlaylist = async (data: {
  owner?: { id: string },
  title: string,
  visibility: 'public' | 'limited'
}) => {
  const {owner, ...rest} = data;

  const _owner = await checkUser(owner);

  const {ref: {id}} = await add(collections.clipsInPlaylist, {
    owner: {ref: _owner.ref},
    refs: [],
    datas: [],
    createdAt: value('serverDate'),
    updatedAt: value('serverDate')
  });
  return set(collections.playlist, id, {
    owner: await checkUser(owner),
    views: 0,
    vtubers: {
      refs: [],
      datas: []
    },
    createdAt: value('serverDate'),
    updatedAt: value('serverDate'),
    ...rest
  });
};

export const updatePlaylist = async (playlistId: string, data: {
  title?: string,
  thumbnailUrl?: string,
  visibility?: 'public' | 'limited'
}) => {
  if (data.title != null || data.visibility != null || data.thumbnailUrl != null) {
    await update(collections.playlist, playlistId, {
      updatedAt: value('serverDate'),
      ...data
    });
  }
};

export const deletePlaylist = async (playlistId: string) => {
  const {remove: _remove, commit} = batch();
  _remove(collections.playlist, playlistId);
  _remove(collections.clipsInPlaylist, playlistId);
  await commit();
};

export const saveClipToPlaylist = async (clipId: string, playlistId: string) => {
  const clipRef = ref(collections.clip, clipId);
  const playlistRef = ref(collections.playlist, playlistId);
  const clipsInPlaylistRef = ref(collections.clipsInPlaylist, playlistId);

  await transaction(
    async ({get: _get}) => {
      const playlist = await _get(playlistRef);
      const clipsInPlaylist = await _get(clipsInPlaylistRef);
      const clipAdded = await _get(clipRef);

      if (playlist === null || clipsInPlaylist === null) {
        throw new Error(`The specified playlist ID is invalid: ${playlistId}`);
      }
      if (clipAdded === null) {
        throw new Error(`The specified clip ID is invalid: ${clipId}`);
      }
      if (clipsInPlaylist.data.datas.length + 1 > MAX_NUM_CLIPS_IN_PLAYLIST) {
        throw new Error(`Maximum number of clips in a playlist exceeded.`);
      }
      return {clipAdded, playlist};
    },
    async ({data, update: _update}) => {

      const {clipAdded} = data;
      const _clipAdded = {
        videoId: clipAdded.data.videoId,
        startSeconds: clipAdded.data.startSeconds,
        endSeconds: clipAdded.data.endSeconds,
        title: clipAdded.data.title,
        description: clipAdded.data.description,
        thumbnailUrl: clipAdded.data.thumbnailUrl,
        vtuberIds: clipAdded.data.vtubers.refs.map(reference => reference.id),
        owner: clipAdded.data.owner,
        createdAt: clipAdded.data.createdAt
      };
      const vtubersAdded = clipAdded.data.vtubers;

      await _update(clipsInPlaylistRef, {
        refs: value('arrayUnion', [clipRef]),
        datas: value('arrayUnion', [_clipAdded]),
        updatedAt: value('serverDate'),
      });
      await _update(playlistRef, [
        field('thumbnailUrl', clipAdded.data.thumbnailUrl),
        field(['vtubers', 'refs'], value('arrayUnion', vtubersAdded.refs)),
        field(['vtubers', 'datas'], value('arrayUnion', vtubersAdded.datas)),
        field('updatedAt', value('serverDate'))
      ]);
    }
  );
};

export const removeClipFromPlaylist = async (clipId: string, playlistId: string) => {
  const clipRef = ref(collections.clip, clipId);
  const playlistRef = ref(collections.playlist, playlistId);
  const clipsInPlaylistRef = ref(collections.clipsInPlaylist, playlistId);

  await transaction(
    async ({get: _get}) => {
      const playlist = await _get(playlistRef);
      const clipsInPlaylist = await _get(clipsInPlaylistRef);
      const clipRemoved = await get<Clip>(collections.clip, clipId);

      if (playlist === null || clipsInPlaylist === null) {
        throw new Error(`The specified playlist ID is invalid: ${playlistId}`);
      }
      if (clipRemoved === null) {
        throw new Error(`The specified clip ID is invalid: ${clipId}`);
      }

      const clipsLeft = removeById(clipsInPlaylist.data, clipId).datas;
      const vtuberIdsLeft = uniq(clipsLeft.flatMap(clip => clip.vtuberIds));
      const vtubersLeft = pickByIds(playlist.data.vtubers, vtuberIdsLeft);

      return {clipRemoved, vtubersLeft};
    },
    async ({data, update: _update}) => {

      const {clipRemoved, vtubersLeft} = data;
      const _clipRemoved = {
        videoId: clipRemoved.data.videoId,
        startSeconds: clipRemoved.data.startSeconds,
        endSeconds: clipRemoved.data.endSeconds,
        title: clipRemoved.data.title,
        description: clipRemoved.data.description,
        thumbnailUrl: clipRemoved.data.thumbnailUrl,
        vtuberIds: clipRemoved.data.vtubers.refs.map(reference => reference.id),
        owner: clipRemoved.data.owner,
        createdAt: clipRemoved.data.createdAt
      };

      await _update(clipsInPlaylistRef, {
        refs: value('arrayRemove', [clipRef]),
        datas: value('arrayRemove', [_clipRemoved]),
        updatedAt: value('serverDate'),
      });
      await _update(playlistRef, {
        vtubers: vtubersLeft,
        updatedAt: value('serverDate'),
      });
    }
  );
};

export const reorderPlaylist = async (playlistId: string, clipIds: string[]) => {
  const clipsInPlaylistRef = ref(collections.clipsInPlaylist, playlistId);

  await transaction(
    async ({get: _get}) => {
      const clipsInPlaylist = await _get(clipsInPlaylistRef);

      if (clipsInPlaylist === null) {
        throw new Error(`The specified playlist ID is invalid: ${playlistId}`);
      }
      return reorder(clipsInPlaylist.data, clipIds);
    },
    async ({data: reorderedClips, update: _update}) => {

      await _update(clipsInPlaylistRef, {
        refs: reorderedClips.refs,
        datas: reorderedClips.datas,
        updatedAt: value('serverDate'),
      });
    }
  );
};
