import {Ref} from 'typesaurus';
import zip from 'lodash/zip';
import unzip from 'lodash/unzip';


interface Container<S, T> {
  refs: Ref<S>[];
  datas: T[];
}

interface Elem<S, T> {
  ref: Ref<S>;
  data: T;
}

type ZippedContainer<S, T> = Elem<S, T>[];

export function doesNotContainNull<S, T>(array: Array<T | null | undefined>): array is T[] {
  return array.every(x => x !== null);
}

function zipContainer<S, T>(container: Container<S, T>): ZippedContainer<S, T> {
  const {refs, datas} = container;
  const zipped = zip<Ref<S>, T>(refs, datas);
  return zipped.map(
    ([ref, data]) => {
      if (ref === undefined || data === undefined) {
        throw new Error(`The properties 'refs' and 'datas' in the given container have different lengths: ${container}`);
      }
      return {ref, data};
    }
  );
}

function unzipContainer<S, T>(container: ZippedContainer<S, T>): Container<S, T> {
  const [refs, datas] = unzip(container.map(
    ({ref, data}) => [ref, data]
  )) as [Ref<S>[], T[]];
  return {refs, datas};
}

export function convertToMap<S, T>(container: ZippedContainer<S, T>): Map<string, Elem<S, T>> {
  return new Map(container.map(elem => [elem.ref.id, elem]));
}

export function convertContainerToMap<S, T>(container: Container<S, T>): Map<string, Elem<S, T>> {
  return convertToMap(zipContainer(container));
}

export function pickByIds<S, T>(container: Container<S, T>, ids: string[]): Container<S, T> {
  const zipped = zipContainer(container);
  const removed = zipped.filter(
    ({ref}) => ids.includes(ref.id)
  );
  return unzipContainer(removed);
}

export function removeByIds<S, T>(container: Container<S, T>, ids: string[]): Container<S, T> {
  const zipped = zipContainer(container);
  const removed = zipped.filter(
    ({ref}) => !ids.includes(ref.id)
  );
  return unzipContainer(removed);
}

export function removeById<S, T>(container: Container<S, T>, id: string): Container<S, T> {
  return removeByIds(container, [id]);
}

export function reorder<S, T>(container: Container<S, T>, ids: string[]): Container<S, T> {
  const fromIdMap = convertContainerToMap(container);
  const reorderedZippedContainer = ids.map(id => fromIdMap.get(id));

  if (!doesNotContainNull(reorderedZippedContainer)) {
    throw new Error(`The given container and IDs have different lengths: ${{container, ids}}`);
  }
  return unzipContainer(reorderedZippedContainer);
}

export function flattenElem<S, T>(elem: Elem<S, T>): T & { id: string } {
  return {...elem.data, id: elem.ref.id};
}

export function flattenContainer<S, T>(contaniner: Container<S, T>): Array<T & { id: string }> {
  return zipContainer(contaniner).map(flattenElem);
}
