import {combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Collection, collection, Doc, get, limit, onGet, order, OrderQuery, query, Query, ref, startAfter, where} from 'typesaurus';

import {Clip, ClipsInPlaylist, Playlist, User, VTuber, WatchHistory} from './schema';
import {convertContainerToMap, convertToMap, flattenContainer, flattenElem} from './utils';
import {mapAsyncIterator} from '../shared/utils';


export const collections = {
  user: collection<User>('user'),
  vtuber: collection<VTuber>('vtuber'),
  clip: collection<Clip>('clip'),
  playlist: collection<Playlist>('playlist'),
  clipsInPlaylist: collection<ClipsInPlaylist>('clipsInPlaylist'),
  watchHistory: collection<WatchHistory>('watchHistory')
};


const _onGet = <T>(collec: Collection<T>, id: string): Observable<Doc<T> | undefined> =>
  new Observable(subscriber =>
    onGet(collec, id,
      doc => doc ? subscriber.next(doc) : undefined,
      subscriber.error
    )
  );

const parseClip = (clip: Doc<Clip>) => ({
  ...clip.data,
  id: clip.ref.id,
  owner: clip.data.owner ? flattenElem(clip.data.owner) : undefined,
  vtubers: flattenContainer(clip.data.vtubers)
});

const parsePlaylist = (playlist: Doc<Playlist>) => ({
  ...playlist.data,
  id: playlist.ref.id,
  owner: playlist.data.owner ? flattenElem(playlist.data.owner) : undefined,
  vtubers: flattenContainer(playlist.data.vtubers)
});

const parsePlaylistAndClips = (playlist: Doc<Playlist>, clips: Doc<ClipsInPlaylist>) => {
  const idToVTuber = convertContainerToMap(playlist.data.vtubers);
  const _clips = flattenContainer(clips.data);
  const clipsVTubersPopulated = _clips.map(clip => ({
    ...clip,
    owner: clip.owner ? flattenElem(clip.owner) : undefined,
    vtubers: clip.vtuberIds.map(id => ({
      ...idToVTuber.get(id).data,
      id
    }))
  }));

  return {...parsePlaylist(playlist), clips: clipsVTubersPopulated};
};

export const getClip = async (id: string) => {
  const clip = await get(collections.clip, id);

  return clip ? parseClip(clip) : undefined;
};

export const getPlaylist = async (id: string) => {
  const playlist = await get(collections.playlist, id);
  const clips = await get(collections.clipsInPlaylist, id);

  return playlist && clips
    ? parsePlaylistAndClips(playlist, clips)
    : undefined;
};

export const onGetPlaylist = (id: string) => {
  const playlist = _onGet(collections.playlist, id);
  const clips = _onGet(collections.clipsInPlaylist, id);

  return combineLatest(playlist, clips).pipe(
    map(([_playlist, _clips]) =>
      playlist && clips
        ? parsePlaylistAndClips(_playlist, _clips)
        : undefined
    )
  );
};

export const listPlaylistsDetailsOfUser = async (id: string) => {
  const playlistDocs = await query(collections.playlist, [
    where(['owner', 'ref'], '==', ref(collections.user, id))
  ]);
  const clipsInPlaylistDocs = await query(collections.clipsInPlaylist, [
    where(['owner', 'ref'], '==', ref(collections.user, id))
  ]);
  const idToDoc = new Map(clipsInPlaylistDocs.map(doc => [doc.ref.id, doc]));
  return playlistDocs.map(playlist => (
    parsePlaylistAndClips(playlist, idToDoc.get(playlist.ref.id))
  ));
};

class PageIterator<T> implements AsyncIterator<Doc<T>[]> {
  constructor(
    private collec: Collection<T>,
    private queries: Query<T, keyof T>[],
    private orderCondition: OrderQuery<T, keyof T>,
    private lastDoc: Doc<T> | undefined = undefined,
    private isDone = false) {
  }

  private nextQuery() {
    const orderBy = order(
      this.orderCondition.field,
      this.orderCondition.method,
      [startAfter(this.lastDoc)]
    );
    return this.queries.concat([orderBy]);
  }

  async next() {
    if (this.isDone) {
      return {done: true, value: undefined};
    }

    const docs = await query(this.collec, this.nextQuery());

    if (docs.length === 0) {
      this.isDone = true;
      return {done: true, value: undefined};
    }

    this.lastDoc = docs[docs.length - 1];
    return {done: false, value: docs};
  }
}

export const listClipsOfUser =
  (id: string, maxNumResults: number) => {
    const queries: Query<Clip, keyof Clip>[] = [
      where(['owner', 'ref'], '==', ref(collections.user, id)),
      limit(maxNumResults)
    ];
    const orderCondition = order<Clip, keyof Clip>('createdAt', 'desc');
    const iterator = new PageIterator(collections.clip, queries, orderCondition);

    return mapAsyncIterator(iterator, docs => docs.map(parseClip));
  };

export const listPlaylistsOfUser =
  (id: string, maxNumResults: number) => {
    const queries: Query<Playlist, keyof Playlist>[] = [
      where(['owner', 'ref'], '==', ref(collections.user, id)),
      limit(maxNumResults)
    ];
    const orderCondition = order<Playlist, keyof Playlist>('createdAt', 'desc');
    const iterator = new PageIterator(collections.playlist, queries, orderCondition);

    return mapAsyncIterator(iterator, docs => docs.map(parsePlaylist)
    );
  };

export const listClipsOfVTubers =
  (ids: string[], maxNumResults: number) => {
    if (ids.length > 10) {
      throw new Error('Number of VTuber IDs must not be greater than 10, due to the limitation of Firestore');
    }

    const vtuberRefs = ids.map(id => ref(collections.vtuber, id));
    const queries: Query<Clip, keyof Clip>[] = [
      where(['vtubers', 'refs'], 'array-contains-any', vtuberRefs),
      where('visibility', '==', 'public'),
      limit(maxNumResults)
    ];
    const orderCondition = order<Clip, keyof Clip>('createdAt', 'desc');
    const iterator = new PageIterator(collections.clip, queries, orderCondition);

    return mapAsyncIterator(iterator, docs => docs.map(parseClip));
  };

export const listRecentClips =
  (maxNumResults: number) => {
    const queries: Query<Clip, keyof Clip>[] = [
      where('visibility', '==', 'public'),
      limit(maxNumResults)
    ];
    const orderCondition = order<Clip, keyof Clip>('createdAt', 'desc');
    const iterator = new PageIterator(collections.clip, queries, orderCondition);

    return mapAsyncIterator(iterator, docs => docs.map(parseClip));
  };
