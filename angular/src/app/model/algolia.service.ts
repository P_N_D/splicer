import {Injectable} from '@angular/core';
import * as algolia from 'algoliasearch';

import {environment} from '../../environments/environment';


const {applicationId, apiKey, index} = environment.algolia;

@Injectable({
  providedIn: 'root'
})
export class AlgoliaService {
  private client = algolia(applicationId, apiKey);
  private vtubersIndex = this.client.initIndex(index);

  constructor() {
  }

  async searchVtubersByName(name: string, maxNumResults: number) {
    const response = await this.vtubersIndex.search({query: name, hitsPerPage: maxNumResults});
    return response.hits.map(record => ({...record, id: record.objectID}));
  }
}
