import {Injectable} from '@angular/core';
import {AngularFireFunctions} from '@angular/fire/functions';

@Injectable({
  providedIn: 'root'
})
export class FirebaseFunctionsService {

  constructor(private fns: AngularFireFunctions) {
  }

  addWatchHistory = (data: { entity: 'clip' | 'playlist', id: string }): Promise<void> => {
    return this.fns.httpsCallable('addWatchHistory')(data).toPromise();
  }
}
