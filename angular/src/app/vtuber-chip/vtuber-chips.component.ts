import {Component, Input, OnInit} from '@angular/core';


interface VTuber {
  name: string;
  avatarUrl: string;
  youtubeId: string;
}

@Component({
  selector: 'app-vtuber-chip',
  templateUrl: './vtuber-chips.component.html',
  styleUrls: ['./vtuber-chips.component.css']
})
export class VtuberChipsComponent implements OnInit {
  @Input() vtubers: VTuber[];
  scrollConfig = {
    suppressScrollY: true
  };

  constructor() {
  }

  ngOnInit() {
  }

}
