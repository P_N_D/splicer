import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtuberChipsComponent } from './vtuber-chips.component';

describe('VtuberChipComponent', () => {
  let component: VtuberChipsComponent;
  let fixture: ComponentFixture<VtuberChipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtuberChipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtuberChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
