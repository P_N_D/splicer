import {NgModule} from '@angular/core';
import {MatChipsModule} from '@angular/material/chips';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';

import {VtuberChipsComponent} from './vtuber-chips.component';


@NgModule({
  declarations: [
    VtuberChipsComponent,
  ],
  imports: [
    MatChipsModule,
    PerfectScrollbarModule,
  ],
  exports: [VtuberChipsComponent]
})
export class VtuberChipModule {
}
