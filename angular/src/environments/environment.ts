// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBhHZLqVgorlbhRVYrZkXeC4LS5IFiGVaw',
    authDomain: 'splicer-dev.firebaseapp.com',
    databaseURL: 'https://splicer-dev.firebaseio.com',
    projectId: 'splicer-dev',
    storageBucket: 'splicer-dev.appspot.com',
    messagingSenderId: '685811767082',
    appId: '1:685811767082:web:b98eef275c29f889a53053',
    measurementId: 'G-DMHFWFVPJY'
  },
  algolia: {
    applicationId: '3EZFJGH75A',
    apiKey: '4b602e5cb33f3239054d94b3a94cc22a',
    index: 'dev_vtubers'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
