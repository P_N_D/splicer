export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyB5PYHyzvVUTEITMmoWFU-tHbeCJNDpVU4',
    authDomain: 'splicer-prod.firebaseapp.com',
    databaseURL: 'https://splicer-prod.firebaseio.com',
    projectId: 'splicer-prod',
    storageBucket: 'splicer-prod.appspot.com',
    messagingSenderId: '77477316917',
    appId: '1:77477316917:web:cc51bc1dff1ebb50e7e47c',
    measurementId: 'G-HF81G9F4VC'
  },
  algolia: {
    applicationId: '3EZFJGH75A',
    apiKey: '4b602e5cb33f3239054d94b3a94cc22a',
    index: 'prod_vtubers'
  }
};
