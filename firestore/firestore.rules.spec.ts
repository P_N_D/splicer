import * as firebase from '@firebase/testing';
import * as fs from 'fs';

/*
 * ============
 *    Setup
 * ============
 */
const projectId = 'firestore-security-rules-test';
const firebasePort = require('../firebase.json').emulators.firestore.port;
const port = firebasePort ? firebasePort : 8080;
const coverageUrl = `http://localhost:${port}/emulator/v1/projects/${projectId}:ruleCoverage.html`;

const rules = fs.readFileSync('firestore.rules', 'utf8');

/**
 * Creates a new app with authentication data matching the input.
 *
 * @param auth the object to use for authentication (typically {uid: some-uid})
 * @return the app.
 */
function authedApp(auth) {
  return firebase.initializeTestApp({projectId, auth}).firestore();
}

/*
 * ============
 *  Test Cases
 * ============
 */
describe('Firestore security rules', () => {
  beforeEach(async () => {
    // Clear the database between tests
    await firebase.clearFirestoreData({projectId});
  });

  beforeAll(async () => {
    await firebase.loadFirestoreRules({projectId, rules});
  });

  afterAll(async () => {
    await Promise.all(firebase.apps().map(app => app.delete()));
    console.log(`View rule coverage information at ${coverageUrl}\n`);
  });

  it('should validate clip data', async () => {
    const db = authedApp(null);
    const profile = db.collection('clip').doc('clip_id');
    await firebase.assertFails(profile.set({
      videoId: 'qQvKq3_MFyY',
      title: 'clip title should be a string',
      description: 'description should be a string',
      views: 0,
    }));
    await firebase.assertSucceeds(profile.set({
      videoId: 'qQvKq3_MFyY',
      title: 'clip title should be a string',
      description: 'description should be a string',
      thumbnailUrl: 'https://img.youtube.com/vi/qQvKq3_MFyY/mqdefault.jpg',
      startSeconds: 5,
      endSeconds: 10,
      views: 0,
      visibility: 'public',
      vtubers: {
        refs: [db.collection('vtuber').doc('vtuber_id')],
        datas: [{
          avatarUrl: 'https://yt3.ggpht.com/a/AGF-l78Dn33A7SSQksXgT6zB0_k2f2ysEz0-_ZOthw=s288-c-k-c0xffffffff-no-rj-mo',
          bio: 'にじさんじ所属バーチャルライバーのリゼ・ヘルエスタです。 ヘルエスタ王国の第二皇女です。#ヘル絵スタ #ヘルエスタ国営放送 #ヘルエスタ王国民 ※タグに投稿いただいた作品は活動に使わせていただくことがあります。',
          name: 'リゼ・ヘルエスタ',
          twitter: {
            avatarUrl: 'https://pbs.twimg.com/profile_images/1212379527021621248/Ir3w1plR_400x400.png',
            bio: 'にじさんじ所属バーチャルライバーのリゼ・ヘルエスタです。 ヘルエスタ王国の第二皇女です。#ヘル絵スタ #ヘルエスタ国営放送 #ヘルエスタ王国民 ※タグに投稿いただいた作品は活動に使わせていただくことがあります。',
            id: '1107935587271467008',
            name: 'Lize_Helesta'
          },
          youtube: {
            avatarUrl: 'https://yt3.ggpht.com/a/AGF-l78Dn33A7SSQksXgT6zB0_k2f2ysEz0-_ZOthw=s288-c-k-c0xffffffff-no-rj-mo',
            bio: 'にじさんじ所属バーチャルライバーのリゼ・ヘルエスタです。\n立派な女王様になるため、毎日お勉強中っ！\n',
            id: 'UCZ1xuCK1kNmn5RzPYIZop3w',
            name: 'リゼ・ヘルエスタ -Lize Helesta-'
          }
        }]
      },
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      updatedAt: firebase.firestore.FieldValue.serverTimestamp()
    }));
  });
});
