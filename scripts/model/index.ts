import {add, batch, collection, ref, value} from 'typesaurus';

import {Clip, ClipsInPlaylist, Playlist, User, VTuber, WatchHistory} from './schema';
import {chunkArray} from './utils';


interface Dates {
  createdAt: Date;
  updatedAt: Date;
}

type Remove<T, U> = Pick<T, Exclude<keyof T, keyof U>>

const maxBatchSize = 500;

export const collections = {
  user: collection<User>('user'),
  vtuber: collection<VTuber>('vtuber'),
  clip: collection<Clip>('clip'),
  playlist: collection<Playlist>('playlist'),
  clipsInPlaylist: collection<ClipsInPlaylist>('clipsInPlaylist'),
  watchHistory: collection<WatchHistory>('watchHistory')
};

export const addVTuber = async (data: Remove<VTuber, Dates>): Promise<VTuber & { id: string }> => {
  const doc = await add(collections.vtuber, {
    ...data,
    createdAt: value('serverDate'),
    updatedAt: value('serverDate')
  });
  return {
    ...doc.data,
    id: doc.ref.id,
  };
};

const _addVTubers = (datas: Remove<VTuber, Dates>[]) => {
  // Don't use the server timestamp of Firestore, since it counts as an extra write operation.
  // https://github.com/firebase/firebase-admin-node/issues/456
  const date = new Date();
  const {set, commit} = batch();

  for (const data of datas) {
    set(ref(collections.vtuber), {
      ...data,
      createdAt: date,
      updatedAt: date
    });
  }

  commit().then();
};

export const addVTubers = (datas: Remove<VTuber, Dates>[]) => {
  const chunks = chunkArray(datas, maxBatchSize - 1);
  for (const chunk of chunks) {
    _addVTubers(chunk);
  }
};
