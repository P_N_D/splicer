/**
 * Returns an array with arrays of the given size.
 */
export function chunkArray<T>(array: T[], chunkSize: number): T[][] {
  const arrays = [];

  for (let i = 0; i < array.length; i += chunkSize) {
    const chunk = array.slice(i, i + chunkSize);
    arrays.push(chunk);
  }

  return arrays;
}
