import {Ref} from 'typesaurus';

type ID<T> = string;

export interface SocialIdentity {
  uid: string;
  providerId: string;
  name: string;
  avatarUrl: string;
}

export interface User {
  name: string;
  avatarUrl?: string;
  bio?: string;
  providerData?: SocialIdentity[];
  createdAt: Date;
  updatedAt: Date;
}

export interface VTuber {
  name: string;
  avatarUrl: string;
  bio: string;
  youtubeId: string;
  twitterId: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface Clip {
  videoId: string;
  startSeconds: number;
  endSeconds: number;
  title: string;
  description: string;
  thumbnailUrl: string;
  visibility: 'public' | 'limited';
  views: number;
  owner?: {
    ref: Ref<User>
    data: User
  };
  vtubers: {
    refs: Ref<VTuber>[]
    datas: VTuber[]
  };
  createdAt: Date;
  updatedAt: Date;
}

export interface ClipInPlaylist {
  videoId: string;
  startSeconds: number;
  endSeconds: number;
  title: string;
  description: string;
  thumbnailUrl: string;
  owner?: {
    ref: Ref<User>
    data: User
  };
  vtuberIds: ID<VTuber>[];
  createdAt: Date;
}

export interface ClipsInPlaylist {
  refs: Ref<Clip>[];
  datas: ClipInPlaylist[];
  owner?: {
    ref: Ref<User>;
  };
  createdAt: Date;
  updatedAt: Date;
}

export interface Playlist {
  title: string;
  thumbnailUrl?: string;
  visibility: 'public' | 'limited';
  views: number;
  owner?: {
    ref: Ref<User>
    data: User
  };
  vtubers: {
    refs: Ref<VTuber>[]
    datas: VTuber[]
  };
  createdAt: Date;
  updatedAt: Date;
}

export interface WatchHistory {
  ipAddress: string;
  user?: Ref<User>;
  target: Ref<Clip | Playlist>;
  createdAt: Date;
}
