import * as fs from 'fs';
import * as parseArgs from 'minimist';
import * as firebase from 'firebase-admin';

import {addVTubers} from '../model';
import {parse, Item} from './parse';


function isEnvName(x: any): x is 'dev' | 'prod' {
  return ['dev', 'prod'].includes(x);
}

const {_: args} = parseArgs(process.argv.slice(2));
if (args.length === 0 || !isEnvName(args[0])) {
  console.error('The environment, either \'dev\' or \'prod\', must be specified.');
  process.exit(1);
}
const env = args[0];

const serviceAccount = require(`../firestore_key.${env}.json`);
firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: `https://splicer-${env}.firebaseio.com`
});

const items = JSON.parse(fs.readFileSync('artifacts/responses.json', 'utf8')) as Item[];
const vtubers = items.map(parse);
addVTubers(vtubers);
