interface YouTubeChannel {
  id: number;
  youtube_channel_id: string;
  subscriber_num: number;
}

export interface Item {
  id: number;
  name: string;
  description: string;
  twitter_id: string;
  image: string;
  youtube_channels: YouTubeChannel[];
}

export function parse(item: Item) {
  return {
    name: item.name,
    avatarUrl: item.image,
    bio: item.description,
    twitterId: item.twitter_id,
    youtubeId: item.youtube_channels[0].youtube_channel_id
  };
}
