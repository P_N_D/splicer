import * as fs from 'fs';

const got = require('got');


interface Response {
  items: any[];
  max_length: number;
  length: number;
}

function sleep(milliSeconds: number) {
  return new Promise(resolve => setTimeout(resolve, milliSeconds));
}

async function* getVTubers(): AsyncIterableIterator<any> {
  const apiUrl = 'https://vtubers-api.herokuapp.com/api/v1/search';
  let i = 1;

  while (true) {
    const params = new URLSearchParams({
      w: 'vtuber',
      page: i.toString()
    });
    console.log(`${apiUrl}\?${params}`);
    const response = await got(`${apiUrl}\?${params}`).json() as Response;

    if (response.items.length === 0) {
      break;
    }

    yield* response.items;
    await sleep(1000);
    i++;
  }
}

async function main() {
  const vtubers = [];

  try {
    for await (const vtuber of getVTubers()) {
      vtubers.push(vtuber);
    }
  } catch (e) {
    console.error(e);
  } finally {
    const json = JSON.stringify(vtubers);
    fs.writeFile('artifacts/responses.json', json, 'utf8', () => undefined);
  }
}

main().then();
